% Henri Menke, Michael Schmid, Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 05.02.2013
%
\renewcommand{\printfile}{2013-02-05}

Wir haben gezeigt dass folgendes gilt:
%
\begin{align*}
  \Ket{B} &= \frac{1}{\sqrt{2}} \left(\Ket{+-} - \Ket{-+}\right) \\
  \sigma_z^{(1)} \sigma_z^{(2)} \Ket{B} &= -\Ket{B} \\
  \Braket{B|\sigma_z^{(1)} \sigma_z^{(2)} | B} &= -1. 
\end{align*}
%
Außerdem zeigt:
\[
 \bm{J}^2 \Ket{B} = 0,
\]
dass $\Ket{B}$ rotationsinvariant ist. Dies führt uns zu:
\[
  \implies \Braket{B|\sigma_{\bm{n}}^{(1)} \sigma_{\bm{-n}}^{(2)}|B} = 1.
\]

\section{Nichtlokalität der Quantenmechanik}

\subsection{Einsteins Einwand (1879-1955) gegen die Quantenmechanik (EPR-Argument 1935, Phys Rev. 47, 777)}

Betrachtet wird der Zerfall $\pi^0 \to e^+ + e^-$:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-3,-0.5)(3,0.5)
    \rput(0,0){
      \psdot*(0,0)
      \psline{->}(0.3,0)(1,0)
      \psline{->}(-0.3,0)(-1,0)
      \uput[90](1,0){\color{DimGray} $e^-$}
      \uput[90](-1,0){\color{DimGray} $e^+$}
    }
    
    \rput(-2,0){
      \psframe(-0.5,-0.5)(0.5,0.5)
      \rput(0,0){\color{DimGray} SG,$A$}
    }
    
    \rput(2,0){
      \psframe(-0.5,-0.5)(0.5,0.5)
      \rput(0,0){\color{DimGray} SG,$B$}
    }
  \end{pspicture}
\end{figure}

Es gelten folgende Tatsachen:
\begin{enum-arab}
  \item $A$ und $B$ entscheiden unabhängig in welche Richtung $\bm{n}_A$ und $\bm{n}_B$ sie den Spin messen:
  \begin{table}[h]
   \centering
      \begin{tabular}{c|c}
          Messung von $A$               & Messung von $B$                   \\ \hline
          $\bm{n}_{A,1} \hateq 1$ & $\bm{n}_{B,1} \hateq \ldots$ \\ 
          $\bm{n}_{A,2} \hateq 1$ & $\bm{n}_{B,2} \hateq \ldots$ \\ 
          $\vdots$                     & $\vdots$                          \\
      \end{tabular}
  \end{table}
  \item Der Befund zeigt, falls $\bm{n}_{A,i} = \bm{n}_{B,i}$ ist, ist das Ergebnis $\sigma_A \sigma_B = -1$
  \item Einstein:
    \begin{enum-roman}
      \item $A$ kann nach der Messung von $\sigma_{\bm{n},A}$ den Wert von Bobs Messung in $\bm{n}$-Richtung exakt voraussagen
      \item Alice Entscheidung welche Richtung $\bm{n}_A$ sie wählt, kann Bobs Spin nicht beeinflussen (Lokalität)
      \item Also muss der Wert von Bobs Spin für jede Richtung $\bm{n}_B$ festgelegt sein, bevor Bob misst
      \item In der Quantenmechanik gilt das nicht.
      \[
        \Braket{B|\sigma_{\bm{n},B}|B} = 0.
      \]
      Daraus folgerte Einstein, dass die Quantenmechanik also unvollständig sein muss
   \end{enum-roman}
\end{enum-arab}

\subsection{Ein Spin $1/2$ mit verborgenen Parametern}
Ein quantenmechanischer Zustand:
\[
  \Ket{\psi} = \Ket{+},
\]
ist alles, was wir wissen. Die Messung von $\sigma_x$ ist also nicht festgelegt.

\begin{notice*}[Einstein:]
Es gibt einen Parameter $\lambda$ verteilt mit $P(\lambda)$, der angibt, welcher Wert bei einer Messung in $\bm{n}$-Richtung herauskommt:
\[
  \tau \left(\bm{n},\lambda|z+\right) = \pm 1.
\]
\end{notice*} 

\begin{notice*}[Aufgabe:] Konstruiere:
\begin{enum-roman}
  \item $\tau(.)$
  \item und $p(\lambda)$ 
\end{enum-roman}
so, dass alle Voraussagen der Quantenmechanik erfüllt sind.
\end{notice*}

Lösung:
\begin{enum-arab}
  \item $\bm{\lambda}$ sei Eigenvektor auf einer Kugel
  \item \[
    \tau\left(\bm{n},\bm{\lambda}|\Ket{z+}\right) = \begin{cases}
      +1 &\text{für $\bm{e}_z \bm{n} > \bm{\lambda}\bm{n}$} \\
      -1 &\text{für $\bm{e}_z \bm{n} < \bm{\lambda}\bm{n}$} 
        \end{cases}
  \]
  \item $p(\lambda)$ sei $1/(4\pi)$ gleich verteilt auf der Kugel
\end{enum-arab}

\begin{figure}[H]
  \centering
  \begin{pspicture}[unit=0.7cm](-1.4,-1.4)(1.4,1.4) %(-2,-2)(2,2)
    \psline{->}(0,-2)(0,2.5)
    \uput[0](0,2.5){\color{DimGray} $z$}
    \psline[linecolor=DarkOrange3]{->}(0,0)(2;45)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](2;45)(2;90)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0,0)(2;-135)
    \psline[linecolor=Purple]{->}(0,0)(2;-60)
    \psline[linecolor=Purple,linestyle=dotted,dotsep=1pt](0.5;-135)(2;-60)
    \psarc{<-}(0,0){1}{45}{90}
    \uput{0.5}[67.5](0,0){\color{DimGray} $\theta$}
    \psarc[linecolor=red](0,0){2}{180}{270}
    \psarc[linecolor=blue](0,0){2}{-90}{180}
    \uput[-45](1;45){\color{DarkOrange3} $\bm{n}$}
    \uput[30](1;-60){\color{Purple} $\bm{\lambda}$}
  \end{pspicture}
\end{figure}

Dann gilt:
%
\begin{align*}
  \Braket{\sigma_n}_{\text{Einstein}} &= \int \tau\left(\bm{n},\bm{\lambda}|\Ket{z+}\right) \cdot p(\bm{\lambda}) \; \mathrm{d}\Omega_\lambda \\
  &= \left(A_{\text{blau}} - A_{\text{rot}}\right) \cdot \frac{1}{4 \pi} \\
  &= \frac{1}{4 \pi} \left(2\pi \left(1+\cos\theta\right) - 2 \pi \left(1-\cos\theta\right)\right) \\
  &= \cos\theta = \text{QM} .
\end{align*}
%

\subsection{Bellsche Ungleichung (1964)}
Bell zeigt, dass die Quantenmechanik und jede Theorie mit verborgenen Parametern (á la 3.2) für 2 Spin $1/2$ Teilchen zu messbaren Differenzen führen muss!

\begin{figure}[H]
  \centering
  \begin{pspicture}(-3,-0.5)(3,0.5)
    \rput(-2.5,0){
      \psframe(-0.5,-0.5)(0.5,0.5)
      \rput(0,0){\color{DimGray} SG,$\bm{n}_A$}
    }
    
    \rput(2.5,0){
      \psframe(-0.5,-0.5)(0.5,0.5)
      \rput(0,0){\color{DimGray} SG,$\bm{n}_B$}
    }
    
    \rput(1,-0.5){
      \psline{->}(0,0)(1;70)
      \psdot*(0.5;70)
      \pnode(0.5;70){A}
    }
    
    \rput(-1,-0.5){
      \psline{->}(0,0)(1;110)
      \psdot*(0.5;110)
      \pnode(0.5;110){B}
    }
    \ncline{A}{B}\ncput*{\psdot(0,0)}
  \end{pspicture}
\end{figure}

\minisec{Einsteins Variante:}
Für jeden Spin ($A$ und $B$) gibt es Funktionen $\tau_i\left(\bm{n}_i,\lambda|\ker{B}\right) = \pm 1$ $(i=A,B)$. Die perfekten Korrelationen lauten:
%
\begin{align}
  \tau_A(\bm{n},\lambda) = -\tau_B(\bm{n},\lambda). \label{eq: Bellllll}
\end{align}
%
Eigenwerte für die Korrelationsmessung:
\begin{enum-arab}
  \item Quantenmechanik:
    \[
      \Braket{B|\sigma_{\bm{n},1}\cdot \sigma_{\bm{n},2}|B} = - \cos\theta
    \]
  ($\theta$ Winkel $\bm{n}_A$ und $\bm{n}_B$)
  \item Einstein:
  %
  \begin{align*}
    E(n_A,n_B) &= \int p(\lambda) \tau_A(\bm{n}_A,\lambda) \tau_B(\bm{n}_B,\lambda) \; \mathrm{d}\lambda 
    \intertext{Unter Verwendung von \autoref{eq: Bellllll} folgt:}
       &= \int -p(\lambda) \tau_A(\bm{n}_A,\lambda) \tau_A(\bm{n}_B,\lambda) \; \mathrm{d}\lambda 
  \end{align*}
  %
\end{enum-arab}
Dann folgt:
%
\begin{align*}
  E(\bm{n},\bm{m}) - E(\bm{n},\bm{l}) &= \int p(\lambda) \left[-\tau(\bm{n},\lambda)\tau(\bm{m},\lambda) + \tau(\bm{n},\lambda) \tau(\bm{l},\lambda)\right] \; \mathrm{d}\lambda  \\
  &= \int \underbrace{p(\lambda)}_{\geq0} \bigg\{\underbrace{-\tau(\bm{n},\lambda)\tau(\bm{m},\lambda)}_{= \pm 1} \underbrace{\big[ 1-\tau(\bm{m},\lambda) \tau(\bm{l},\lambda) \big]}_{\geq 0} \bigg\} \; \mathrm{d}\lambda  
\end{align*}
%
\begin{notice*}
Mit:
\[
\left|\sum x_i\right| \leq \sum |x_i|
\]
folgt:
\end{notice*}
%
\begin{align*}
  \left|E(\bm{n},\bm{m}) - E(\bm{n},\bm{l})\right| &\leq \int p(\lambda) \left(1- \tau(\bm{m},\lambda) \tau(\bm{l},\lambda)\right) \\
  &= 1+ E(\bm{m},\bm{l})
\end{align*}
%
Dies ist die \acct{Bellsche Ungleichung für zwei Spin $1/2$ Teilchen}.

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(1,1)
    \psline{->}(0,0)(1;90)
    \uput[180](1;90){\color{DimGray} $\bm{n}$}
    \psline{->}(0,0)(1;60)
    \uput[60](1;60){\color{DimGray} $\bm{m}$}
    \psline{->}(0,0)(1;30)
    \uput[-60](1;30){\color{DimGray} $\bm{\ell}$}
    \psarc{<-}(0,0){0.6}{60}{90}
    \uput{0.3}[75](0,0){\color{DimGray} $\theta$}
    \psarc{<-}(0,0){0.6}{30}{60}
    \uput{0.3}[45](0,0){\color{DimGray} $\theta$}
  \end{pspicture}
\end{figure}

Für $E$ die quantenmechanischen Eigenwerte einsetzten:
\[
  \left|-\cos\theta + \cos(2\theta)\right| \leq \left(1- \cos\theta\right) .
\]

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.3,-1.5)(4,1.5)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.3,-1.5)(4,1.5)[\color{DimGray} $\theta$,0][,0]
    \psplot{0}{\psPi}{-cos(0.5*x)}
    \psplot{0}{\psPi}{cos(x)}
    \psplot[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt]{0}{\psPi}{-cos(0.5*x)+cos(x)}
    \psplot[linecolor=DarkOrange3]{0}{\psPi}{abs(-cos(0.5*x)+cos(x))}
    \psplot[linecolor=MidnightBlue]{0}{\psPi}{1-cos(0.5*x)}
    \psyTick(1){\color{DimGray} 1}
    \psyTick(-1){\color{DimGray} -1}
    \psxTick(\psPi){\color{DimGray} \frac{\pi}{2}}
    \uput[45](\psPi,1){\color{DarkOrange3} linke Seite}
    \uput[-45](\psPi,1){\color{MidnightBlue} rechte Seite}
  \end{pspicture}
\end{figure}

Experimente zeigen, dass die Quantenmechanik richtig ist (Aspect 1982-\ldots). Eine Theorie mit lokalen verborgenen Parametern ist also ausgeschlossen! 
