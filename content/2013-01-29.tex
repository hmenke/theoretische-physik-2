% Henri Menke, Michael Schmid, Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 29.01.2013
%
\renewcommand{\printfile}{2013-01-29}

\section{Bornsche Näherung}

\begin{notice*}[Erinnerung:]
  Bei eindimensionalen Streuproblemen an Potentialen haben wir Wellenfunktionen, Reflexions- und Transmissionskoeffizienten berechnet.
  
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-1.5,-0.3)(1.5,1.5)
      \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-0.3)(1.5,1.5)[\color{DimGray} $x$, 0][\color{DimGray} $V(x)$, 0]
      \psline(-1.5,0.2)(0.5,0.2)(0.5,1.0)(0.8,1.0)
      \pscurve(0.8,1.0)(1.0,0.8)(1.2,1.2)
      \psline(1.2,1.2)(1.5,1.2)
      \pnode(-1,0.6){A}
      \pnode(-0.3,0.6){B}
      \ncline[arrows=->]{A}{B} %\naput{\color{DimGray} $\ket{\psi}$}
      \uput[90](-0.65,0.6){\color{DimGray} $\ket{\psi}$}
    \end{pspicture}
  \end{figure}
\end{notice*}

Jetzt betrachten wir solche Probleme in drei Dimensionen. Es fällt eine ebene Wellenfront, die an einem Potential gestreut wird ein. Im Nahfeld um das Potential bildet sich eine auslaufende Kugelwelle.
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-0.8)(4.5,1)
    \psline(-1,-0.5)(-1,0.5)
    \psline(-0.8,-0.5)(-0.8,0.5)
    \psline(-0.6,-0.5)(-0.6,0.5)
    \psline(-0.4,-0.5)(-0.4,0.5)
    \psline(-0.2,-0.5)(-0.2,0.5)
    
    \pscircle(1.5,0){0.2}
    \pscircle(1.5,0){0.4}
    \pscircle(1.5,0){0.6}
    \pscircle(1.5,0){0.8}
    
    \rput(1.5,0){
      \psdot*(0,0)
      \psline{->}(0,0)(1.5;30)
      \psline{->}(0,0)(1.5;0)
      \psarc{->}(0,0){1}{0}{30}
      \uput[15](1;15){\color{DimGray} $\theta$}
      \uput[-90](1.5;0){\color{DimGray} $z$}
      \uput[30](1.5;30){\color{DimGray} auslaufend}
    }
    
    \rput(-0.5,0.7){
      \pnode(-0.5,0){A}
      \pnode(0.5,0){B}
      \ncline[arrows=->]{A}{B} %\naput{\color{DimGray} einfallend}
      \uput[90](0,0){\color{DimGray} einfallend}
    }
  \end{pspicture}
\end{figure}
%
Die gesamte Wellenfunktion besteht also aus einem einfallenden und einem auslaufenden Teil.
%
\begin{align*}
  \phi(\bm{r})
  &= \phi_{\text{in}}(\bm{r}) + \phi_{\text{ex}}(\bm{r}) \\
  &= \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} + \phi_{\text{ex}}(\bm{r}) \\
  &= \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} + f(\theta) \frac{\mathrm{e}^{\mathrm{i} k r}}{r}
\end{align*}
%
Wir suchen nun $f(\theta)$. Diese Funktion wird nun von der Form des Objekts/Potentials abhängen, an dem gestreut wird. Betrachte die stationäre Schrödingergleichung.
%
\begin{align*}
  \left( - \frac{\hbar^2}{2 m} \nabla^2 + V(r) \right) \phi(\bm{r}) &= E \phi(\bm{r})
\end{align*}
%
Definiere
%
\begin{gather*}
  E = \frac{\hbar^2}{2 m} k^2 \qquad v = \frac{2 m}{\hbar^2} V
\end{gather*}
%
Damit
%
\begin{align*}
  (\nabla^2 + k^2) \phi(\bm{r}) &= v(r) \phi(\bm{r})
\end{align*}

\minisec{Vorgehen}

\begin{description}
  \item[1. Green's Funktion für die linke Seite]
  %
  \begin{align*}
    (\nabla^2 + k^2) G(\bm{r},\bm{r}') &= \delta(\bm{r} - \bm{r}')
  \end{align*}
  %
  dann gilt
  %
  \begin{align*}
    \phi(\bm{r}) &= \int \mathrm{d}\bm{r}' \; \Big( G(\bm{r},\bm{r}') \, v(r') \, \phi(\bm{r}') \Big) + \phi^{(0)}(\bm{r})
  \end{align*}
  %
  wobei $\phi^{(0)}(\bm{r})$ eine Lösung der homogenen Gleichung ist.
  
  \item[2. Störungstheorie in $v(r)$] Im Fall $v = 0$:
  %
  \begin{align*}
    \phi(\bm{r}) \overset{!}{=} \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} = \phi^{(0)}(\bm{r})
  \end{align*}
  %
  Für $v \neq 0$ führen wir eine Störungstheorei in erster Ordnung in $v$ durch.
  %
  \begin{align*}
    \phi(\bm{r}) &= \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} + \int \mathrm{d}\bm{r}' \; G(\bm{r},\bm{r}') \, v(r') \, \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}'} + \mathcal{O}(v^2)
  \end{align*}
  %
  Hier sind wir fertig, sobald wir $G(\bm{r},\bm{r}')$ berechnet haben.
  
  \item[3. Bestimmung der Green's Funktion] Die rechte Seite von
  %
  \begin{align*}
    (\nabla^2 + k^2) G(\bm{r},\bm{r}') &= \delta(\bm{r} - \bm{r}')
  \end{align*}
  %
  hängt nur von der Differenz $\bm{r} - \bm{r}'$ ab, folglich wird die linke Seite auch nur von dieser Differenz abhängen.
  %
  \begin{align*}
    (\nabla^2 + k^2) G(\bm{r} - \bm{r}') &= \delta(\bm{r} - \bm{r}')
  \end{align*}
  %
  Substituiere: $\bm{u} = \bm{r} - \bm{r}'$. Schließlich Fourier-transformieren wir die Gleichung
  %
  \begin{align*}
    \int \mathrm{d}^3 u \; \mathrm{e}^{\mathrm{i} \bm{q} \cdot \bm{u}} (\nabla_u^2 + k^2) G(\bm{u}) &= 1
  \intertext{Zweimaliges partielles Integrieren führt auf die Form}
    (k^2 - q^2) G(\bm{q}) &= 1
  \end{align*}
  %
  wobei
  %
  \begin{align*}
    G(\bm{q}) &= \int \mathrm{d}^3 u \; \mathrm{e}^{\mathrm{i} \bm{q} \cdot \bm{u}} G(\bm{u})
  \end{align*}
  %
  Also folgt sofort
  %
  \begin{align*}
    G(\bm{q}) &= \frac{1}{k^2 - q^2}
  \end{align*}
  %
  Jetzt haben wir $G$ gefunden. Leider haben wir $G$ derzeit im $q$-Raum vorliegen, wir möchten $G$ jedoch im $u$-Raum (Ortsraum) haben, also müssen wir die Fourier-Rücktransformation durchführen
  %
  \begin{align*}
    G(\bm{u})
    &= \int \frac{\mathrm{d}^3 q}{(2 \pi)^3} \frac{1}{k^2 - q^2} \mathrm{e}^{\mathrm{i} \bm{q} \cdot \bm{u}} \\
    &= \frac{2 \pi}{(2 \pi)^3} \int\limits_{0}^{\infty} q^2 \mathrm{d}q \int\limits_{-1}^{1} \mathrm{d}(\cos \theta) \; \frac{1}{k^2 - q^2} \mathrm{e}^{\mathrm{i} q u \cos \theta} \\
    &= \frac{1}{4 \pi^2} \int\limits_{0}^{\infty} \mathrm{d}q \; q^2 \frac{1}{k^2 - q^2} \frac{1}{\mathrm{i} q u} \left( \mathrm{e}^{\mathrm{i} q u} - \mathrm{e}^{- \mathrm{i} q u} \right) \\
    &= \frac{1}{4 \pi^2} \frac{1}{u} \int\limits_{-\infty}^{\infty} \mathrm{d}q \; \frac{q^2}{k^2 - q^2} \frac{1}{\mathrm{i} q} \mathrm{e}^{\mathrm{i} q u}
  \end{align*}
  %
  Nun haben wir jedoch ein Problem; wenn $k^2 = q^2$ divergiert hier der Integrand. Abhilfe bietet jedoch eine Integration mit Hilfe des Residuensatzes. Da die beiden Pole auf dem Rand des Integrationsgebiets liegen müssen sie verschoben werden. Dabei wird einer der beiden ins Integrationsgebiet aufgenommen, der andere wird herausgeschoben.
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-1.5,-0.3)(1.5,1.5)
      \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-0.3)(1.5,1.5)[\color{DimGray} Re, 0][\color{DimGray} Im, 0]
      \psdots*[dotstyle=x](-0.5,0)(0.5,0)
      \psdots*[linecolor=DarkOrange3,dotstyle=x](-0.5,-0.1)(0.5,0.1)
      \psline[linecolor=DarkOrange3](-1.2,0)(-0.6,0)
      \psarc[linecolor=DarkOrange3](-0.5,0){0.1}{0}{180}
      \psline[linecolor=DarkOrange3](-0.4,0)(0.4,0)
      \psarc[linecolor=DarkOrange3](0.5,0){0.1}{180}{0}
      \psline[linecolor=DarkOrange3](0.6,0)(1.2,0)
      \psarc[linecolor=DarkOrange3](0,0){1.2}{0}{180}
      \uput{0.3}[-90](-0.5,0){\color{DimGray} $-k$}
      \uput{0.3}[-90](0.5,0){\color{DimGray} $k$}
    \end{pspicture}
  \end{figure}
  %
  Faktorisierung
  %
  \begin{align*}
    k^2 - q^2 = - (q-k)(q+k)
  \end{align*}
  %
  Durch die oben genannt Verschiebung der Pole muss nur der Pol bei $+k$ beachtet werden. An dieser Stelle liegt ein Pol erster Ordnung vor.
  %
  \begin{align*}
    \mathrm{Res}(f,k)
    &= \lim\limits_{q \to k} \frac{1}{0!} \frac{\mathrm{d}^0}{\mathrm{d}q^0} \left( (q - k)^1 \frac{q^2}{- (q-k)(q+k)} \frac{1}{\mathrm{i} q} \mathrm{e}^{\mathrm{i} q u} \right) \\
    &= \lim\limits_{q \to k} \left( \frac{q^2}{-(q+k)} \frac{1}{\mathrm{i} q} \mathrm{e}^{\mathrm{i} q u} \right) \\
    &= \frac{k^2}{-(k+k)} \frac{1}{\mathrm{i} k} \mathrm{e}^{\mathrm{i} k u} \\
    &= - \frac{1}{2 \mathrm{i}} \mathrm{e}^{\mathrm{i} k u}
  \end{align*}
  %
  Damit
  %
  \begin{align*}
    \frac{1}{4 \pi^2} \frac{1}{u} \int\limits_{-\infty}^{\infty} \mathrm{d}q \; \frac{q^2}{k^2 - q^2} \frac{1}{\mathrm{i} q} \mathrm{e}^{\mathrm{i} q u}
    &= 2 \pi \mathrm{i} \frac{1}{4 \pi^2} \frac{1}{u} \mathrm{Res}(f,k) \\
    &= - 2 \pi \mathrm{i} \frac{1}{4 \pi^2} \frac{1}{u} \frac{1}{2 \mathrm{i}} \mathrm{e}^{\mathrm{i} k u} \\
    &= - \frac{1}{4 \pi u} \mathrm{e}^{\mathrm{i} k u}
  \end{align*}
  %
  Eingesetzt in die Lösung erster Ordnung ergibt sich schlussendlich
  %
  \begin{align*}
    \phi(\bm{r}) &= - \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \frac{1}{4 \pi} \int \mathrm{d}\bm{r}' \; \frac{\mathrm{e}^{\mathrm{i} k |\bm{r} - \bm{r}'|}}{|\bm{r} - \bm{r}'|} \, v(r') \, \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}}
  \end{align*}
\end{description}

\minisec{Fernfeldnäherung für kurzreichweitiges Potential}

Wir nehmen an $v(r)$ sei kurzreichweitig. Betrachte also für $|\bm{r}| \gg |\bm{r}'|$ die Entwicklung
%
\begin{align*}
  |\bm{r} - \bm{r}'|
  &= \sqrt{\bm{r}^2 - 2 \bm{r} \cdot \bm{r}' + {\bm{r}'}^2} \\
  &\approx r \left( 1 - \frac{\bm{r}' \cdot \bm{e}_r}{r} + \mathcal{O}\left( \frac{{r'}^2}{r^2} \right) \right)
\end{align*}
%
Mit dieser Entwicklung verändern sich die Terme des Integranden
%
\begin{align*}
  \frac{\mathrm{e}^{\mathrm{i} k |\bm{r} - \bm{r}'|}}{|\bm{r} - \bm{r}'|}
  \xleftarrow{\text{Näherung}}
  \frac{\mathrm{e}^{\mathrm{i} k r}}{r} \mathrm{e}^{\mathrm{i} k \bm{r}' \cdot \bm{e}_r}
\end{align*}
%
Im Folgenden sind $\bm{k}_i = k$, $|k|_f = |\bm{k}| \bm{e}_r$ und $|\bm{k}_i| = |\bm{k}_f| = |\bm{k}|$, dann
%
\begin{align*}
  \frac{\mathrm{e}^{\mathrm{i} k r}}{r} \mathrm{e}^{\mathrm{i} k \bm{r}' \cdot \bm{e}_r} = \frac{\mathrm{e}^{\mathrm{i} k r}}{r} \mathrm{e}^{\mathrm{i} \bm{k}_f \cdot \bm{r}'}
\end{align*}
%
Damit
%
\begin{align*}
  \phi(\bm{r}) &= - \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} - \frac{\mathrm{e}^{\mathrm{i} k r}}{4 \pi r} \underbrace{\int \mathrm{d}^3\bm{r}' \; v(r') \, \mathrm{e}^{\mathrm{i} (\bm{k}_i - \bm{k}_f) \cdot \bm{r}'}}_{\eqcolon f(\theta)}
\end{align*}
%
$f(\theta)$ wird auch Streuamplitude genannt. Die Streuamplitude ist die Fouriertransformierte des Potentials mit dem Impulsübertrag $\bm{k}_i - \bm{k}_f$.

\begin{theorem*}[Definition]
  Differentieller Wirkungsquerschnitt
  %
  \begin{align*}
    \frac{\mathrm{d}\sigma}{\mathrm{d}\Omega} &= |f(\theta)|^2
  \end{align*}
\end{theorem*}

Im Falle eine kugelsymmetrischen Potentials gilt
%
\begin{align*}
  v(q)
  &= \int \mathrm{d}^3 r' \; \mathrm{e}^{\mathrm{i} \bm{q} \cdot \bm{r}} v(r') \\
  &= 2 \pi \int\limits_{0}^{\infty} \mathrm{d}r' \; {r'}^2 \int\limits_{-1}^{1} \mathrm{d}(\cos \theta) \; v(r') \mathrm{e}^{\mathrm{i} q r' \cos \theta} \\
  &= 2 \pi \int\limits_{0}^{\infty} \mathrm{d}r' \; {r'}^2 \, v(r') \frac{\mathrm{e}^{\mathrm{i} q r'} - \mathrm{e}^{-\mathrm{i} q r'}}{\mathrm{i} q r'} \\
  &= \frac{4 \pi}{q} \int\limits_{0}^{\infty} \mathrm{d}r' \; {r'}^2 \, v(r') \frac{\sin(q r')}{r'} \\
  &= \frac{4 \pi}{q} \int\limits_{0}^{\infty} \mathrm{d}r' \; r' \, v(r') \sin(q r')
\end{align*}

\begin{example*} Abgeschirmtes Coulomb-Potential (dimensionslos)
  %
  \begin{align*}
    v(r) &= \frac{1}{r} \mathrm{e}^{-r/r_0}
  \end{align*}
  %
  Dann lautet
  %
  \begin{align*}
    v(q) &= \frac{4 \pi}{q} \int\limits_{0}^{\infty} \mathrm{d}r' \; r' \, \sin(q r') \, \frac{1}{r'} \mathrm{e}^{-r'/r_0}
  \intertext{schreibt man den Sinus in Exponentialdarstellung lassen sich die Exponentialfunktionen zusammenfassen und integrieren}
    &= 4 \pi \frac{1}{q^2 + \left( \frac{1}{r_0} \right)^2} \\
    &\to \frac{4 \pi}{q^2} \qquad (r \to \infty)
  \end{align*}
  %
  In diesem Fall ist der differentielle Wirkungsquerschnitt
  %
  \begin{align*}
    \frac{\mathrm{d}\sigma}{\mathrm{d}\Omega} &= |f(\theta)|^2 = \frac{e^2}{4 m^2 \left( \frac{\hbar k}{m} \right)^2 \sin^4 \left( \frac{\theta}{2} \right)}
  \end{align*}
\end{example*}

\begin{notice*}[Bemerkungen:]
  \begin{enumerate}
    \item Bornsche Näherung ist sogar exakt. Dies ist die Formel für die >>Rutherford-Streuung<<.
    
    \item Für das Coulomb-Potential ergeben sich klassisch und quantenmechanisch derselbe Wirkungsquerschnitt.
  \end{enumerate}
\end{notice*}











