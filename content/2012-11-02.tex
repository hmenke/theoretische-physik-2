% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung vom 2.11.2012
%
\renewcommand{\printfile}{2012-11-02}

\section{Dynamik}

\subsection{Dynamisches Postulat (P4) oder die Schrödinger Gleichung}
Nach einer Präparation und vor einer Messung entwickelt sich ein quantales System gemäß der \acct{Schrödinger-Gleichung}:
\[
  \boxed{\mathrm{i} \hbar \frac{\mathrm{d}}{\mathrm{d}t} \ket{\psi} = H \ket{\psi}}
\]
mit dem \acct{Hamilton-Operator} $H$ (immer hermitesch, eventuell $H(t)$) und 
\[
  \hbar = \frac{h}{2 \pi} = 1.055 \cdot 10^{-34} \, \mathrm{Js}
\] 
einer neuen Fundamental-Konstante. $h$ entspricht dem Plankschen Wirkungsquantum.

\begin{notice*}
  \begin{enum-arab}
    \item Für Systeme mit klassischen Analogon entspricht $H$ der Hamilton-Funktion
    \item vgl. klassisch: \[\dot{q}_i = \frac{\partial H}{\partial p_i} \text{ und } \dot{p}_i=-\frac{\partial H}{\partial q_i}\]
    \item Erst hier kommt $\hbar$ ins Spiel. Es vermittelt den Zusammenhang zwischen Energie und Zeit
  \end{enum-arab}
\end{notice*}

\subsection{Beispiel: Spin \texorpdfstring{$1/2$}{1/2} im konstantem Magnetfeld}

\minisec{Klassisch:}

Das magnetische Momente $\bm{\mu}$ im $B$-Feld $\bm{B}$ hat die Energie:
\[
  E = \bm{\mu} \cdot \bm{B}
\]

\minisec{Quantal:}
%
\begin{align*}
  \bm{\mu} &= - g \frac{\mu_B }{\hbar} \bm{S} = -g \mu_B \frac{1}{2} \bm{\sigma} \\
  H &= -\bm{\mu} \cdot \bm{B} = g \mu_B \frac{1}{2} \bm{\sigma} \cdot \bm{B} \stackrel{\bm{B}=B_z \bm{e}_z}{=} \frac{\hbar}{2} \omega \sigma_z 
\end{align*}
%
Wobei hier aber nur der Spin-Anteil betrachtet wurde. Außerdem gilt:
\[
  \omega = \frac{g \mu_B B_z}{\hbar}
\] 

\minisec{Schrödinger-Gleichung}
Die Schrödinger-Gleichung lautet somit:
%
\begin{align*}
  \mathrm{i} \hbar \frac{\mathrm{d}}{\mathrm{d}t} \ket{\psi(t)} &= \frac{\hbar}{2} \omega \sigma_z \ket{\psi(t)} 
  \intertext{mit:}
  \ket{\psi(t)} &\hateq \begin{pmatrix} c_+(t)  \\ c_-(t)  \end{pmatrix} \\
  \implies \mathrm{i} \hbar \frac{\mathrm{d}}{\mathrm{d}t} \begin{pmatrix} c_+(t)  \\ c_-(t)  \end{pmatrix}  &= \frac{\hbar}{2} \omega \begin{pmatrix} 1 & 0  \\ 0 & -1  \end{pmatrix} \begin{pmatrix} c_+(t) \\ c_-(t)  \end{pmatrix} \\
  \mathrm{i} \partial_t c_{\pm}(t) &= \frac{\omega}{2} (\pm 1) c_{\pm} (t) \\
  \implies c_+(t) &= \exp\left(-\frac{\mathrm{i} \omega}{2} (t-t_0)\right) c_+(t_0) \\
  c_-(t) &= \exp\left(\frac{\mathrm{i} \omega}{2} (t-t_0)\right) c_-(t_0) 
\end{align*}
%
\begin{align*}
  \implies \ket{\psi(t)} &= \exp\left(-\frac{\mathrm{i} \omega}{2} (t-t_0)\right) c_+(t_0)  \ket{z+} +  \exp\left(\frac{\mathrm{i} \omega}{2} (t-t_0)\right) c_-(t_0)  \ket{z-}
\end{align*}
%
Somit folgt für die Wahrscheinlichkeiten:
%
\begin{align*}
  \text{prob}[\sigma_z \hateq +1 | \ket{\psi(t)}] &= |\Braket{z+|\psi(t)}|^2 = |c+(t_0)|^2
\end{align*}
%
Diese Wahrscheinlichkeit ist also unabhängig von der Zeit, aber:
%
\begin{align*}
  & \text{prob}[\sigma_x \hateq 1 | \ket{\psi(t)}] = |\Braket{x+|\psi(t)}|^2 \\
  =& \left| \exp\left(-\frac{\mathrm{i} \omega}{2} (t-t_0)\right) c_+(t_0) \frac{1}{\sqrt{2}}  + \exp\left(\frac{\mathrm{i} \omega}{2} (t-t_0)\right) c_-(t_0) \frac{1}{\sqrt{2}}  \right|^2
\end{align*}
%
für $\ket{\psi(t)}= \ket{x+}$ folgt:
%
\begin{align*}
  c_+(t_0) &= \frac{1}{\sqrt{2}}  \\
  c_-(t_0) &= \frac{1}{\sqrt{2}}
\end{align*}
%
Für die Wahrscheinlichkeit gilt dann:
%
\begin{align*}
  \text{prob}[\sigma_x \hateq 1 | \ket{\psi(t)}] &= \frac{1}{4} \left( 2 \cos\left( \frac{\omega}{2}(t-t_0) \right) \right)^2
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.2)(3.5,1)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.2)(3.5,1)[\color{DimGray} $t$,0][\color{DimGray},0]
    \psplot[linecolor=DarkOrange3]{0}{3.5}{0.5*cos(1.57079632679*x)^2}
    \uput[-90](2,0){\color{DimGray} $2 \pi / \omega$}
    \uput[-135](0,0){\color{DimGray} $t_0$}
    \uput[180](0,0.5){\color{DimGray} $1$}
    \psline(-0.1,0.5)(0.1,0.5)
    \psline[linestyle=dotted,dotsep=1pt](2,0)(2,1)
  \end{pspicture}
\end{figure}

\minisec{Mittelwert:}
%
\begin{align*}
  \Braket{\sigma_x}_{\psi(t)} &= \Braket{\psi(t)|\sigma_x|\psi(t)} \\
  &\stackrel{t_0=0}{=} \frac{1}{2} \begin{pmatrix} \exp\left(\mathrm{i} \frac{\omega}{2} t\right) & \exp\left(-\mathrm{i} \frac{\omega}{2} t\right) \end{pmatrix} \cdot  \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} \cdot  \begin{pmatrix} \exp\left(-\mathrm{i} \frac{\omega}{2} t\right) \\ \exp\left(\mathrm{i} \frac{\omega}{2} t\right) \end{pmatrix} \\
  &= \frac{1}{2} \left( \exp(-\mathrm{i} \omega t) + \exp(\mathrm{i} \omega t) \right) \\
  &= \cos(\omega t)
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.6)(3.5,1)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.6)(3.5,1)[\color{DimGray} $t$,0][\color{DimGray} $\Braket{\sigma_x}_{\Ket{\psi(t)}}$,0]
    \psplot[linecolor=DarkOrange3]{0}{3.5}{0.5*cos(1.57079632679*x)}
    \uput[180](0,0.5){\color{DimGray} $1$}
    \psline(-0.1,0.5)(0.1,0.5)
    \uput[180](0,-0.5){\color{DimGray} $-1$}
    \psline(-0.1,-0.5)(0.1,-0.5)
  \end{pspicture}
\end{figure}

d.h. der Mittelwert (auch Erwartungwert) in $x$-Richtung präzediert mit der Lamor-Frequenz $\omega$ um die $B$-Feld Achse.

\subsection{Spin \texorpdfstring{$1/2$}{1/2} im rotierendem Magnetfeld (Rabi-Oszillatoren)} 

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.5)(1.5,1)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.2)(1.5,1)
    \psline{->}(0,0)(0.8,0.8)
    \psline[linecolor=DarkOrange3]{->}(0,0)(0.8,0)
    \psline[linecolor=DarkOrange3]{->}(0,0)(0,0.8)
    \uput[180](0,0.6){\color{DarkOrange3} $B_0$}
    \uput[-60](0.8,0){\color{DarkOrange3} $B_1$}
    \psellipse[linestyle=dotted,dotsep=1pt,linecolor=DarkOrange3](0,0)(0.8,0.4)
    \psellipticarc[linecolor=DarkOrange3,arrows=->](0,0)(0.8,0.4){20}{70}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

\begin{align*}
  \bm{B}(t) &= B_0 \bm{e}_z + B_1 (\cos(\omega t) \bm{e}_x + \sin(\omega t) \bm{e_y}) \\
  \implies H(t) &= \frac{\hbar \omega_0}{2} \sigma_z + \frac{\hbar \omega_1}{2} (\cos(\omega t) \sigma_x + \sin(\omega t) \sigma_y)
\end{align*}
%
Somit lautet die Schrödinger-Gleichung:
%
\begin{align*}
  \mathrm{i} \begin{pmatrix} \dot{c}_+ \\ \dot{c}_- \end{pmatrix} &= \begin{pmatrix} \frac{\omega_0}{2}  & \frac{\omega_1}{2} \cos(\omega t) - \mathrm{i} \frac{\omega}{2} \sin(\omega t)   \\ \frac{\omega_1}{2} \cos(\omega t) + \mathrm{i} \frac{\omega}{2} \sin(\omega t)  &  -\frac{\omega_0}{2} \end{pmatrix} \cdot \begin{pmatrix} c_+(t) \\c_-(t) \end{pmatrix}
\end{align*}
%
Für die einzelnen Komponenten $\mathrm{i}\dot{c}_+$ und $\mathrm{i}\dot{c}_-$ folgt:
%
\begin{align*}
  \mathrm{i}\dot{c}_+ &= \frac{\omega_0}{2} c_+ + \frac{\omega_1}{2} \exp(-\mathrm{i} \omega t) c_-\\
  \mathrm{i}\dot{c}_- &= \frac{\omega_1}{2} \exp(\mathrm{i} \omega t) c_+ - \frac{\omega_0}{2}  c_-
\end{align*}
%
Für den Übergang ins mitbewegte Bezugssystem wird der Term $b_\pm$ wie folgt definiert:
%
\begin{align*}
  b_\pm &\coloneq \exp\left(\pm \frac{1}{2} \mathrm{i} \omega t\right) c_\pm .
\end{align*}
%
Für die Komponenten bedeutet dies:
%
\begin{align*}
  \mathrm{i} \dot{b}_+ &= -\frac{\omega - \omega_0}{2} b_+ + \frac{\omega_1}{2} b_- \\
  \mathrm{i} \dot{b}_- &= \frac{\omega_1}{2} b_+ + \frac{\omega - \omega_0}{2} b_- \\\
  \implies \ddot{b}_\pm &= -\left( \frac{\Omega}{2}\right)^2 b_\pm \qquad \text{mit $\Omega^2 = (\omega-\omega_0^2) + \omega_1^2$}
\end{align*}
%
Prinzipiell wäre das Problem damit gelöst (Das Lösen der DGL ist trivial).

Sei nun $\Ket{\psi(0)} = \Ket{z+}$. Somit folgt:
%
\begin{align*}
  \implies c_+(0) &= 1 \qquad c_-(0)=0 \\
  \implies b_+(0) &= 1 \qquad b_-(0)=0 \\
  \implies b_-(t) &= a  \cos\left(\frac{\Omega}{2}\right) + b \sin\left(\frac{\Omega}{2}\right) \\
  b_-(0) &= a \\
  \dot{b}_-(0) &= \frac{b \Omega}{2} \\
  \implies b_-(t) &= b_-(0) \cos\left(\frac{\Omega}{2} t\right) + \frac{2 \dot{b}_-(0)}{\Omega} \sin\left(\frac{\Omega}{2} t\right)
\end{align*}
%
dafür $\dot{b}_- = \mathrm{i} \omega b_+(0) + \dot{c}_-(0) = {\omega_1}/{2 \mathrm{i}}$. Somit folgt weiter:
%
\begin{align*}
  \implies b_-(t) &= - \frac{\mathrm{i} \omega}{\Omega} \sin\left(\frac{\Omega}{2} t\right) \\
  c_-(t) &= \exp\left( \frac{\mathrm{i} \omega t}{2} \right) \left(-\left(\frac{-\mathrm{i} \omega_1}{\Omega}\right)\right) \sin\left(\frac{\Omega}{2} t\right)
\end{align*}
%
Für die Wahrscheinlichkeit folgt somit:
%
\begin{align*}
  \text{prob}[\sigma_z \hateq -1|\Ket{\psi(t)}] &= |c_-(t)|^2 = \left( \frac{\omega_1}{2} \right)^2 \sin^2 \left( \frac{\Omega}{2} t \right)
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.2)(3.5,1)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.2)(3.5,1)[\color{DimGray} $t$,0][\color{DimGray},0]
    \psplot[linecolor=DarkOrange3]{0}{3.5}{0.5*sin(1.57079632679*x)^2}
    \uput[-90](2,0){\color{DimGray} $2 \pi / \omega$}
    \uput[-135](0,0){\color{DimGray} $t_0$}
    \uput[180](0,0.5){\color{DimGray} $(\omega_1/\Omega)^2$}
    \psline(-0.1,0.5)(0.1,0.5)
    \psline[linestyle=dotted,dotsep=1pt](2,0)(2,1)
  \end{pspicture}
\end{figure}

Resonanz liegt vor, wenn: $\omega = \omega_0$, d.h. $\Omega=\omega_1$, d.h das $B_1$-Feld. Daraus lässt sich folgern, dass der Spin selbst bei infinitesimalem $B_1$ beginnt zu flippen!

\begin{notice*}[Rabi 1939] 
Magnetisches Moment des Protons durch Variaion von $B_0$ (Nobelpreis 1944)
\end{notice*}

\begin{example*}[Anwendung]
Kernspintomographie (NMR)
\end{example*} 
