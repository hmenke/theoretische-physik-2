% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 26.10.2012
%
\renewcommand{\printfile}{2012-10-26}

\subsection{Operatorfunktionen}

Sei $A$ ein Operator, dann ist $f(A)$ definiert als:
%
\begin{enumerate}
  \item $\displaystyle f(A) = \sum\limits_{n=0}^{\infty} \frac{1}{n!} A^n \left(f^{(n)}(0)\right)$ (Potenzreihe)
  \item Falls $A$ hermitesch gilt:
  %
  \begin{align*}
    \sum\limits_{n} a_n \Ket{n}\Bra{n} &= A 
  \end{align*}
  %
  Dies ist die Spektraldarstellung \acct[0]{ohne Entartung}!
  %
  \begin{align*}
    A \Ket{n} &= a_n \Ket{n} \\
    f(A) &\coloneq \sum\limits_{n} f(a_n) \Ket{n}\Bra{n}
  \end{align*}
  %
  \acct[0]{Mit Entartung} gilt:
  \begin{align*}
    A &= \sum\limits_{n} \sum\limits_{r_1}^{g(n)} a_n \Ket{n,r}\Bra{n,r} \\
    \implies f(A) &= \sum\limits_{n} \sum\limits_{r_1}^{g(n)} f(a_n) \Ket{n,r}\Bra{n,r}
  \end{align*}
\end{enumerate}


\begin{example*}
  \begin{align*}
    f(x) &= \exp(x) = \sum\limits_{n=0}^{\infty} \frac{x^n}{n!} \qquad x \in \mathbb{C} \\
    f(A) &= \exp(A) = \sum\limits_{n=0}^{\infty} \frac{A^n}{n!}
  \end{align*}
\end{example*}

\section{Prinzipien der Quantenmechanik}

\subsection{Zustands- und Superpositionspostulat (P1)}

Bei vollständiger Kenntnis des Zustandes eines quantalen Systems wird dieses durch einen normierten Vektor $\Ket{\psi}$ im zugehörigen Hilbertraum $\mathcal{H}$ repräsentiert. Mit $\Ket{\psi_1}$ und $\Ket{\psi_2}$ ist auch die Superposition 
\[
  \Ket{\psi} = c_1 \Ket{\psi_1} + c_2 \Ket{\psi_2}
\]
(bei geeigneter Normierung) ein möglicher Zustand.

\begin{notice*}
  \begin{enumerate}
    \item Vergleiche klassisch: $(q_i,p_i)$ im Phasenraum $\left(q_i^{(1)} + q_i^{(2)},p_i^{(1)} + p_i^{(2)} \right)$ ist klassisch \acct[0]{Unsinn}!
    \item >>vollständige Kenntnis<< ist hier wichtig, was bei unvollständiger Kenntnis gilt, kommt später.
  \end{enumerate}
\end{notice*}

\subsection{Operator-, Mess-, Präparations-Postulat}

\begin{theorem*}[P2a]
  Jeder physikalischen Größe $A$ entspricht ein hermitischer Operator $A$
\end{theorem*}

\begin{theorem*}[P2b]
  Eine Messung der Größe $A$ im Zustand $\Ket{\psi_0}$ ergibt mit Sicherheit einen der reellen Eigenwerte von $A$, also $A\Ket{j} = a_j \Ket{j}$. Die Wahrscheinlichkeit genau $a_n$ zu messen ist
  \begin{align*}
    \mathrm{prob}[A\hateq a_n | \Ket{\psi_0}] &=
    \begin{dcases}
      |\Braket{n|\psi_0}|^2 \ \ \text{falls $a_n$ nicht entartet} \\
      \sum\limits_{r=1}^{g(n)} |\Braket{n,r|\psi_0}|^2 \ \ \text{falls $a_n$ entartet}
    \end{dcases} \\
    %
    &= \Braket{\psi_0|P_n|\psi_0} \text{ mit }
    \begin{dcases}
      P_n = \Ket{n} \Bra{n} \\
      P_n = \sum\limits_{r=1}^{g(n)} \Ket{n,r} \Bra{n,r}
    \end{dcases}
  \end{align*} 
\end{theorem*}

\begin{theorem*}[P2c]
  Unmittelbar nach der Messung von $A$ mit Messwert $a_n$ ist das System im Zustand
  \begin{align*}
    \Ket{\psi} &= \frac{P_n \Ket{\psi_0}}{\|P_n \Ket{\psi_0}\|} \\
    &= 
    \begin{dcases}
      \Ket{n} \\
      \sum\limits_{r=1}^{g(n)} \Ket{n,r} \Braket{n,r|\psi_0} 
    \end{dcases}
  \end{align*}
\end{theorem*}

\begin{notice*}
  In der klassischen Sicht sind physikalische Größen Phasenraumfunktionen
  %
  \begin{align*}
    f \left(\{q_i\},\{p_i\}\right) = f(\{\xi_i\})
  \end{align*}
  %
  zum Beispiel $\left( r_{i\alpha}, p_{i\alpha}, L_\alpha^{\text{ges}}, E\right)$, wobei die Messung nichts ändert.
\end{notice*}

\subsection{Spezialfall: \texorpdfstring{>>Born'sche Regel<<}{Born'sche Regel}}

Sei $A$ die Eigenschaft >>in Zustand $\Ket{\psi}$ zu sein<<. Der zugehörige Operator ist $P_\chi = \Ket{\chi} \Bra{\chi}$ mit den Eigenwerten 
%
\begin{align*}
  P_\chi \Ket{\phi} \overset{!}{=} a\Ket{\phi} \quad \implies \quad \Ket{\chi}\Braket{\chi|\phi} = a\Ket{\phi} .
\end{align*}
%
Dies gilt nur, falls $\Ket{\chi} = \Ket{\phi}$ also $a=1$. Bei sonstigen Eigenvektoren $a=0$.

Die Wahrscheinlichkeit $1$ zu messen ist im Zustand $\Ket{\psi_0}$ gegeben durch
%
\begin{align*}
  \mathrm{prob}\left[P_\chi \hateq 1 \Big| \Ket{\psi_0} \right] = |\Braket{\chi|\psi_0}|^2 .
\end{align*} 

\subsection{Erwartungswert vs. Messwert (nicht entartet)}

Die Einzelmessung ergibt $a_n$ mit Wahrscheinlichkeit
%
\begin{align*}
  P_n \coloneq |\Braket{n|\psi_0}|^2
\end{align*}
%
Zur experimentellen Bestimmung von $P_n$ muss oft an gleichen (aber nicht am selben, also am identisch präparierten) Zustand $\Ket{\psi}$ gemessen werden. Der Mittelwert vieler solcher Messungen ist
%
\begin{align*}
  \Braket{A}_{\psi_0} &= \sum\limits_{n} P_n a_n \\
  &=\sum\limits_{n} a_n \Braket{n|\psi_0}\Braket{\psi_0|n} \\
  &=\sum\limits_{n} \Braket{\psi_0|n} a_n \Braket{n|\psi_0} \\
  &=\Braket{\psi_0|A|\psi_0}
\end{align*}
%
Diese letzte Größe heißt \acct{Erwartungswert} des Operators $A$ im Zustand $\Ket{\psi_0}$. Erwartungswert ist eine schlechte (aber historisch etablierte) Bezeichnung, da dieser Wert bei einer einzelnen Messung gar nicht gemessen werden kann.

Den Mittelwert der Abweichung vom Mittelwert nennt man Dispersion, sie lautet:
%
\begin{align*}
  (\Delta A)_{\psi_0}^2 &= \sum\limits_{n} P_n (a_n - \Braket{A}_{\psi_0})^2 \\
  &= \sum\limits_{n} \Braket{\psi_0|n}\Braket{n|\psi_0} \left(a_n^2 - 2 a_n \Braket{A}_{\psi_0} + \Braket{A}_{\psi_0}^2\right) \\
  &= \sum\limits_{n} \Braket{\psi_0|n} a_n^2 \Braket{n|\psi_0} - 2 \Braket{A}_{\psi_0} \sum\limits_{n} \Braket{\psi_0|n} a_n \Braket{n|\psi_0} + \Braket{A}_{\psi_0}^2 \\
  &= \Braket{\psi_0|A^2|\psi_0} - 2 \Braket{A}_{\psi_0}^2 + \Braket{A}_{\psi_0}^2 \\
  &= \Braket{A^2}_{\psi_0} - 2 \Braket{A}_{\psi_0}^2 + \Braket{A}_{\psi_0}^2 \\
  &= \Braket{A^2}_{\psi_0} - \Braket{A}_{\psi_0}^2
\end{align*}

\subsection{Heisebergsche Unschärferelation}

\index{Heisenbergsche Unschärferelation}

\begin{theorem*}[Unschärferlation] 
  Seien $A$ und $B$ physikalsche Größen (d.h. hermitesche Operatoren) mit:
  %
  \begin{align*}
    [A,B] &= AB-BA = \mathrm{i}C
  \end{align*}
  %
  Es gilt, $C$ ist hermitesch:
  %
  \begin{align*}
    [A,B]^\dagger &= (AB-BA)^\dagger = B^\dagger A^\dagger - A^\dagger B^\dagger \\
    &= [B^\dagger, A^\dagger] 
  \end{align*}
  \begin{align*}
    [B^\dagger, A^\dagger] &\stackrel{\text{hermitesch}}{=} [B,A] = -[A,B]
  \end{align*}
  %
  das heißt,
  %
  \begin{align*}
    C &= (-\mathrm{i}[A,B]) \\
    \implies C^\dagger &= (-\mathrm{i}[A,B])^\dagger \\
    &= \mathrm{i} [A,B]^\dagger \\
    &= - \mathrm{i}[A,B] \\
    &= -\mathrm{i}(\mathrm{i}C) = C
  \end{align*}
  %
  Dann gilt für die Dispersion (>>Unschärfe<<) von $A$ und $B$ Messungen:
  %
  \begin{align*}
    (\Delta A)_{\psi_0} \cdot (\Delta B)_{\psi_0} \geq \frac{1}{2} |\Braket{C}_{\psi_0}| 
  \end{align*}
  %
  \begin{proof}
    Folgende Definitionen gelten:
    %
    \begin{align*}
      A_0 &\coloneq A- \Braket{A}_{\psi_0} \mathds{1} \\
      B_0 &\coloneq B- \Braket{B}_{\psi_0} \mathds{1} 
    \end{align*}
    %
    Somit folgt:
    %
    \begin{align*}
      \implies [A_0,B_0] &= \mathrm{i}C  \\
      (\Delta A)_{\psi_0}^2 &= (\Delta{A_0})_{\psi_0}^2 = \Braket{A_0^2}_{\psi_0} \\
      (\Delta B)_{\psi_0}^2 &= (\Delta{B_0})_{\psi_0}^2 = \Braket{B_0^2}_{\psi_0}
    \end{align*}
    %
    \begin{align*}
      0  &\leq |(A_0 + \mathrm{i} \lambda B_0)\Ket{\psi_0}|^2 \\
      &= |A_0 \Ket{\psi_0}|^2 + \mathrm{i} \lambda \Braket{\psi_{0}|A_{0} B_{0}|\psi_{0}} - \mathrm{i} \lambda \Braket{\psi_0|B_0A_0|\psi_0} + \lambda^2  |B_{0} \Ket{\psi_0}|^2\\
      &= \Braket{A_0^2}_{\psi_0} + \lambda^2 \Braket{{B_0}^2}_{\psi_0} \qquad \forall \lambda \in \mathbb{R}  \\
      \implies \Braket{C}_{\psi_0}^2 &\leq 4 \cdot \Braket{{A_0}^2}_{\psi_0} \Braket{{B_0}^2}_{\psi_0}
    \end{align*}
  \end{proof}
\end{theorem*}

\begin{notice*}
  Die Messung nicht kommutierender Variablen kann nicht beliebig scharf (dispersionsfrei) werden.
\end{notice*}

