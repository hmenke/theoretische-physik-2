% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 11.12.2012
%
\renewcommand{\printfile}{2012-12-11}

\begin{notice*}[Rückblick:]
  Kapitel I: Spin $1/2$ und ein $N$-Niveau System
  
  Kapitel II: $1$ Freiheitsgrad in $\dim = 1$
\end{notice*}

\begin{notice*}[Ziel:]
  Wir wollen Systeme mit zwei und mehr Teilchen beschreiben. Diese haben mehrere Freiheitsgrade, z.B. Spin- und Ortsfreiheitsgrad.
\end{notice*}

\section{Zusammengesetzte Systeme}

\subsection{Beispiel: Zwei unterschiedliche Teilchen}

Betrachte zwei unterschiedliche Teilchen in einer Raumdimension. Sie bewegen sich im Potential
%
\begin{align*}
  V(x_1,x_2) = V_1(x_1) + V_2(x_2) + V_{\text{int}}(x_1,x_2)
\end{align*}
%
wobei $V_{\text{int}}(x_1,x_2)$ das Wechselwirkungspotential der beiden Teilchen ist. Betrachten wir das Problem auf dem Niveau der Wellenfunktion.
%
\begin{align*}
  \psi(x_1,x_2) \text{ mit } \varrho(x_1,x_2) = |\psi(x_1,x_2)|^2
\end{align*}
%
$\varrho(x_1,x_2)$ ist die Wahrscheinlichkeit das erste Teilchen bei $x_1$ und gleichzeitig das zweite bei $x_2$ zu finden. Die Wahrscheinlichkeit das erste Teilchen bei $x_1$ zu finden ist
%
\begin{align*}
  \varrho(x_1) = \int \mathrm{d}x_2 \; \varrho(x_1,x_2) = \int \mathrm{d}x_2 \; |\psi(x_1,x_2)|^2 .
\end{align*}
%
Das zweite Teilchen wurde >>ausintegriert<< oder >>ausgespart<<. Für die Normierung gilt nun:
%
\begin{align*}
  1 = \int \mathrm{d}x_1 \; \varrho(x_1) = \int \mathrm{d}x_2 \; \varrho(x_2) = \int \mathrm{d}x_1 \int \mathrm{d}x_2 \; \varrho(x_1,x_2)
\end{align*}
%
Betrachte die zeitabhängige Schrödingergleichung in Ortsdarstellung:
%
\begin{multline*}
  \mathrm{i} \hbar \partial_t \psi(x_1,x_2,t)
  = H \psi(x_1,x_2,t) \\
  = \left[ - \frac{\hbar}{2 m} \partial_{x_1} - \frac{\hbar}{2 m} \partial_{x_2} + V_1(x_1) + V_2(x_2) + V_{\text{int}}(x_1,x_2) \right] \psi(x_1,x_2,t)
\end{multline*}

\minisec{Stationäre Lösungen}

\paragraph{Fall 1} $H$ ist separabel $\iff$ $V_{\text{int}} = 0$
%
\begin{align*}
  \left[ - \frac{\hbar}{2 m} \partial_{x_1} - \frac{\hbar}{2 m} \partial_{x_2} + V_1(x_1) + V_2(x_2) + V_{\text{int}}(x_1,x_2) \right] \phi(x_1,x_2) &= E \phi(x_1,x_2)
\end{align*}
%
Wir wählen einen Produktansatz: $\phi(x_1,x_2) = \phi_{E_1}(x_1) \phi_{E_2}(x_2)$.
%
\begin{multline*}
    \phi_{E_2}(x_2) \left[ - \frac{\hbar}{2 m} \partial_{x_1} + V_1(x_1) \right] \phi_{E_1}(x_1)  \\
    \shoveright
    + \phi_{E_1}(x_1) \left[ - \frac{\hbar}{2 m} \partial_{x_2} + V_2(x_2) \right] \phi_{E_2}(x_2) = E \phi_{E_1}(x_1) \phi_{E_2}(x_2) \\
    \shoveleft
    \frac{1}{\phi_{E_1}(x_1)} \left[ - \frac{\hbar}{2 m} \partial_{x_1} + V_1(x_1) \right] \phi_{E_1}(x_1) \\
    + \frac{1}{\phi_{E_2}(x_2)} \left[ - \frac{\hbar}{2 m} \partial_{x_2} + V_2(x_2) \right] \phi_{E_2}(x_2) = E
\end{multline*}
%
Auf der rechten Seite steht nur eine Energie (skalar). Das heißt die beiden Terme auf der linken Seite müssen auch Energien sein.
%
\begin{align*}
  \underbrace{\frac{1}{\phi_{E_1}} \left[ - \frac{\hbar}{2 m} \partial_{x_1} + V_1(x_1) \right] \phi_{E_1}}_{\stackrel{!}{=} E_1}
  +
  \underbrace{\frac{1}{\phi_{E_2}} \left[ - \frac{\hbar}{2 m} \partial_{x_2} + V_2(x_2) \right] \phi_{E_2}}_{\stackrel{!}{=} E_2}
  = E
\end{align*}

\paragraph{Fall 2} $H$ ist nicht separabel. Wir erhalten eine partielle Differentialgleichung, die nur in wenigen speziellen Fällen lösbar ist.

\paragraph{Fall 3} Wenn das Wechselwirkungspotential nur eine Funktion des Abstandes der Teilchen ist, also
%
\begin{align*}
  V(x_1,x_2) = V(x_1 - x_2)
\end{align*}
%
und $V_1 = V_2 = 0$, dann können wir analog zur klassischen Mechanik auf Schwerpunktskoordinaten transformieren.
%
\begin{align*}
  x_{\mathrm{CM}} &= \frac{m_1 x_1 + m_2 x_2}{m_1 + m_2} \\
  p_{\mathrm{CM}} &= p_1 + p_2
\end{align*}
%
Der Kommutator von $x_{\mathrm{CM}}$ und $p_{\mathrm{CM}}$ ist gegeben als
%
\begin{align*}
  [x_{\mathrm{CM}},p_{\mathrm{CM}}] = \mathrm{i} \hbar .
\end{align*}
%
Die Relativkoordinate lautet
%
\begin{align*}
  x &= x_1 - x_2 \\
  p &= \mu \left( \frac{p_1}{m_1} + \frac{p_2}{m_2} \right) . \qquad {\color{DimGray} \frac{1}{\mu} = \frac{1}{m_1} + \frac{1}{m_2}}
\end{align*}
%
Für die Relativkoordinaten lautet der Kommutator ebenfalls
%
\begin{align*}
  [x,p] = \mathrm{i} \hbar .
\end{align*}
%
Unter den obigen Annahmen ist der Hamiltonoperator
%
\begin{align*}
  H = \frac{p_1^2}{2 m_1} + \frac{p_2^2}{2 m_1} + V(x_1-x_2) .
\end{align*}
%
Mit den neuen Koordinaten transformiert dieser zu
%
\begin{align*}
  H = \underbrace{\frac{p_{\mathrm{CM}}}{2 (m_1 + m_2)}}_{\text{freie Bewegung}} + \underbrace{\frac{p^2}{2 \mu} + V(x)}_{\text{1d Problem}} .
\end{align*}
%
Das Problem ist also separabel.

\subsection{Hilbertraum als Tensorprodukt}

Unter Voraussetzung zweier endlich dimensionaler Systeme seien
%
\begin{enum-arab}
  \item $\mathcal{H}^{(1)}$ mit Basis $\{ \Ket{n} , n = 1,\ldots,N \}$
  \item $\mathcal{H}^{(2)}$ mit Basis $\{ \Ket{m} , m = 1,\ldots,N \}$
\end{enum-arab}

\begin{example*}
  System 1: Benzolring auf dem ein Elektron kreist
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-0.5,-0.5)(0.5,0.5)
      \psline(0.5;30)(0.5;90)(0.5;150)(0.5;210)(0.5;270)(0.5;330)(0.5;390)
      \psdots(0.5;30)(0.5;90)(0.5;150)(0.5;210)(0.5;270)(0.5;330)(0.5;390)
      \uput[30](0.5;30){\color{DimGray} $1$}
      \uput[90](0.5;90){\color{DimGray} $2$}
      \uput[150](0.5;150){\color{DimGray} $3$}
      \uput[210](0.5;210){\color{DimGray} $4$}
      \uput[270](0.5;270){\color{DimGray} $5$}
      \uput[330](0.5;330){\color{DimGray} $6$}
    \end{pspicture}
    \vspace*{-4em}
  \end{figure}
  %
  \begin{align*}
    \Ket{\psi_1} = \sum\limits_{n = 1}^{6} c_n \Ket{n}
  \end{align*}
  %
  System 2: Spin des Elektrons
  %
  \begin{align*}
    \Ket{\psi_2} = \sum\limits_{m = 1}^{2} d_m \Ket{m} = (d_{+} \Ket{z+} + d_{-} \Ket{z-})
  \end{align*}
\end{example*}

Der Gesamtraum ist
%
\begin{align*}
  \mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)}.
\end{align*}
%
Dieser hat die Basis
%
\begin{align*}
  \{ \Ket{n} \otimes \Ket{m} , n = 1,\ldots,N \wedge m = 1,\ldots,M \} \qquad {\color{DimGray} \dim \mathcal{H} = N \cdot M}
\end{align*}

\begin{example*}[\ForwardToEnd \hspace*{0.5em} Im Beispiel]
  Basis $\{ \Ket{1} \otimes \Ket{z+} , \Ket{1} \otimes \Ket{z-} , \Ket{2} \otimes \Ket{z+} , \ldots \}$
\end{example*}

Ein beliebiger Zustand in $\mathcal{H}$ ist dann gegeben durch
%
\begin{align*}
  \Ket{\psi} = \sum\limits_{n,m} a_{nm} \Ket{n} \otimes \Ket{m} = \sum\limits_{j = 1}^{N \cdot M} a_j \Ket{j}
\end{align*}

\begin{notice*}
  Nicht jedes $\Ket{\psi} \in \mathcal{H}$ lässt sich schreiben als $\Ket{\psi_1} \otimes \Ket{\psi_2}$.
  
  Beispiel: 
  %
  \begin{align*}
    \Ket{\psi}
    &= \frac{1}{\sqrt{2}} \left( \Ket{1} \otimes \Ket{z+} + \Ket{2} \otimes \Ket{z-} \right) \\
    &\neq \left( \sum\limits_{n} c_n \Ket{n} \right) \otimes \left( \sum\limits_{m} d_m \Ket{m} \right) \\
    &\neq \Ket{\psi_1} \otimes \Ket{\psi_2}
  \end{align*}
  %
  Dieses Phänomen heißt >>Verschränkung<< (engl.: entanglement).
\end{notice*}

\minisec{Skalarprodukt}

\begin{align*}
  \left( \Bra{n_1'} \otimes \Bra{n_2'} \right) \left( \Ket{n_1} \otimes \Ket{n_2} \right)
  &= \Braket{n_1' | n_1} \Braket{n_2' | n_2} \\
  &= \delta_{n_1' n_1} \delta_{n_2' n_2}
\end{align*}
%
Das heißt für zwei Vektoren in der Basis mit Tensorprodukt:
%
\begin{align*}
  \Ket{\psi} &= \sum\limits_{n,m} a_{nm} \Ket{n} \otimes \Ket{m} \\
  \Ket{\phi} &= \sum\limits_{n',m'} b_{n' m'} \Ket{n'} \otimes \Ket{m'} \\
  \Braket{\phi | \psi} &= \sum\limits_{n,m} a_{nm} a_{nm} b_{nm}^*
\end{align*}
%
Betrachtet man den kontinuierlichen Fall mit zwei Systemen:
%
\begin{item-triangle}
  \item Teilchen im System 1
  
  $\Ket{\psi_1} \in \mathcal{H}^{(1)}$ mit Basis $\{ \Ket{x_1} \}$, $\{ \Ket{p_1} \}$ oder $\{ \Ket{n_1} \}$.
  
  \item Teilchen im System 2
  
  >>genauso<<
\end{item-triangle}
%
Dann ist der Gesamtzustand gegeben durch
%
\begin{align*}
  \Ket{\psi} \in \mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)}
\end{align*}
%
mit Basis
%
\begin{align*}
  &\{ \Ket{x_1} \otimes \Ket{x_2} \hateq \Ket{x_1, x_2} \} \\
  \text{oder} \quad &\{ \Ket{p_1} \otimes \Ket{p_2} \hateq \Ket{p_1, p_2} \} \\
  \text{oder} \quad &\{ \Ket{x_1} \otimes \Ket{p_2} \}
\end{align*}
%
Das heißt für die Wellenfunktion
%
\begin{align*}
  \Ket{\psi}
  &= \int \mathrm{d}x_1 \int \mathrm{d}x_2 \; \psi(x_1,x_2) \Ket{x_1,x_2} \\
  &= \int \mathrm{d}x_1 \sum\limits_{n_2 = 0}^{\infty} \psi_{n_2}(x_1) \Ket{x_1,n_2}
\end{align*}

\subsection{Operatoren}

Wir wählen zwei Systeme $\mathcal{H}^{(1)}$ und $\mathcal{H}^{(2)}$, wobei
%
\begin{item-triangle}
  \item$\mathcal{H}^{(1)}$ mit Operatoren $A^{(1)}$
  
  \item$\mathcal{H}^{(2)}$ mit Operatoren $A^{(2)}$
\end{item-triangle}
%
Um die beiden Systeme zu verknüpfen braucht $A^{(1)}$ einen Partner (Ausdehnung) auf $\mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)}$
%
\begin{align*}
  A^{(1)} \to A^{(1) \otimes (2)} = A^{(1)} \otimes \mathds{1}^{(2)} .
\end{align*}

\begin{example*} Ortsoperator für System 1
  \begin{align*}
    \hat{x}^{(1)} \Ket{x_1} &= x_1 \Ket{x_1} \\
    \hat{x}^{(1) \otimes (2)} \Ket{x_1, x_2} &= x_1 \Ket{x_1, x_2} \\
  \end{align*}
\end{example*}

Allgemein gilt
%
\begin{align*}
  (A^{(1)} \otimes B^{(2)}) \Ket{\psi}
  &= (A^{(1)} \otimes B^{(2)}) (\Ket{\psi_1} \otimes \Ket{\psi_2}) \\
  &= (A^{(1)} \Ket{\psi_1}) \otimes (B^{(2)} \Ket{\psi_2}) .
\end{align*}
%
Vereinfachung der Notation
%
\begin{align*}
  \Ket{n_1} \otimes \Ket{m_2} \to \Ket{n_1,m_2}
\end{align*}
%
der dazu adjungierte Vektor lautet
%
\begin{align*}
  \Bra{n_1,m_2}
\end{align*}
%
und für den Operator
%
\begin{align*}
  A^{(1) \otimes (2)} = A^{(1)} \otimes \mathds{1}^{(2)} \to A^{(1)}
\end{align*}

\begin{example*}[\ForwardToEnd \hspace*{0.5em} Im Beispiel]
  2 Teilchen in einer Dimension
  %
  \begin{align*}
    \mathrm{i} \hbar \partial_t \Ket{\psi} &= \left[ \frac{\hat{p}_1^2}{2 m_1} + \frac{\hat{p}_2^2}{2 m_2} + V_1(x_1) + V_2(x_2) \right] \Ket{\psi}
  \end{align*}
  %
  Teile den Hamilton-Operator auf in
  %
  \begin{align*}
    H_1 &= \frac{\hat{p}_1^2}{2 m_1} + V_1(x_1) \\
    H_2 &= \frac{\hat{p}_2^2}{2 m_2} + V_2(x_2)
  \end{align*}
  %
  dann gilt für die beiden
  %
  \begin{align*}
    [H_1,H_2] = 0
  \end{align*}
\end{example*}

Seien die Eigenzustände und Energieeigenwerte zu beiden Systemen gegeben.
%
\begin{align*}
  H^{(1)} \Ket{E_i^{(1)}} = E_i^{(1)} \Ket{E_i^{(1)}} \\
  H^{(2)} \Ket{E_j^{(2)}} = E_j^{(2)} \Ket{E_j^{(2)}} \\
\end{align*}
%
Im Gesamtraum $\mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)}$ ergibt das
%
\begin{align*}
  \Ket{E_{ij}}
  &= \Ket{E_i^{(1)}} \otimes \Ket{E_j^{(2)}} =  \Ket{E_i^{(1)}, E_j^{(2)}} \\
  H \Ket{E_{ij}}
  &= (H^{(1)} \otimes \mathds{1}^{(2)} + \mathds{1}^{(1)} \otimes H^{(2)}) (\Ket{E_i^{(1)}} \otimes \Ket{E_j^{(2)}}) \\
  &= E_i^{(1)} (\Ket{E_i^{(1)}} \otimes \Ket{E_j^{(2)}}) + E_j^{(2)} (\Ket{E_i^{(1)}} \otimes \Ket{E_j^{(2)}}) \\
  &= \underbrace{(E_i^{(1)} + E_j^{(2)})}_{E_{ij}} (\Ket{E_i^{(1)}} \otimes \Ket{E_j^{(2)}}) \\
  &= E_{ij} \Ket{E_{ij}} .
\end{align*}
%
Der allgemeinste zeitabhängige Zustand lautet
%
\begin{align*}
  \Ket{\psi} = \sum\limits_{i,j} c_{ij} \exp\left( -\frac{\mathrm{i}}{\hbar} E_{ij} t \right) \Ket{E_{ij}}
\end{align*}
%
wobei die $c_{ij}$ über die Anfangsbedingungen festgelegt sind.
