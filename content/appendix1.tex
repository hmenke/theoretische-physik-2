% Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\renewcommand{\printfile}{appendix1}

\section{Etwas zu den infinitesimalen Erzeugern}
%
\subsection{Das Stone Theorem}
%
Ist ein Satz unitärer Operatoren gegeben, die von einem kontinuierlichen Parameter $\alpha$ abhängen und die abelschen Gruppeneigenschaften
%
\begin{align*}
  U(\alpha_1 + \alpha_2) &= U(\alpha_1) U(\alpha_2) , \qquad U(0) = \mathds{1}
\end{align*}
%
erfüllen, so existiert ein hermitescher Operator $T$, der als infinitesimaler Erzeuger der Transformationsgruppe $U(\alpha)$ bezeichnet wird, sodass
%
\begin{align*}
  U(\alpha) &= \exp(\mathrm{i} \alpha T)
\end{align*}
%
Da $T$ hermitesch ist, ist er ein guter Kandidat dafür, um physikalische Eigenschaften zu beschreiben. Es können folgende Zusammenhänge zwischen den infinitesimalen Erzeugern für diverse Transformationen und deren physikalischen Eigenschaften hergestellt werden:
%
\begin{itemize}
  \item Zeitentwicklung (siehe Zeitentwicklungsoperator)
  \item Translationen: $U(\bm{a})= \exp(-\mathrm{i} a (\bm{p}\cdot \hat{a})/\hbar)$. Der Operator $T_h= \bm{p} \cdot  \hat{a}$ ist die Komponente des Impulses $\bm{p}$ entlang $\hat{a}$.
  \item Rotationen um  einen Winkel $\theta$ um eine Achse $\hat{n}$: $U_{\hat{n}}(\theta) =  \exp(-\mathrm{i} \theta (\bm{J} \cdot \hat{n})/\hbar)$. Der Operator $T_h$ ist die Komponente des Drehimpulses $\bm{J}$ entlang $\hat{n}$.
\end{itemize}
%
\subsection{Kommutatorrelationen der infinitesimalen Erzeuger}
%
Im folgenden soll die Drehgruppe betrachtet werden. Der Operator $R_{\hat{n}}(\theta)$, der um einen Winkel $\theta$ um die Achse $\hat{n}$ dreht, ist ein orthogonaler Operator des dreidimensionalen Raums: $R^\top R = R R^\top = \mathds{1}$. Die Drehungen $R_{\hat{n}}(\theta)$ bilden eine abelsche Untergruppe der Drehgruppe. Gemäß des \acct{Stone Theorems} kann, wie oben gesehen, stets geschrieben werden:
%
\begin{align}
  R_{\hat{n}}(\theta) &= \exp(-\mathrm{i} \theta (\bm{T} \cdot \hat{n})), \label{eq:Stone_Theorem}
\end{align}
%
wobei $\bm{T} \cdot \hat{n}$ ein hermitescher Operator ist. Da $R$ orthogonal und reell ist, ist er auch unitär. Betrachte eine Drehung mit Winkel $\theta$ um die $\hat{n}(\phi)$-Achse in der $yz$-Ebene.
%
\begin{figure}[h]
  \centering
  \begin{pspicture}(-1,-0.5)(3,2)
    \psline{->}(0,0)(3,0)
    \psline{->}(0,0)(0,2)
    \psline{->}(0,0)(-1,-0.5)
    \uput[-90](-1,-0.5){\color{DimGray} $x$}
    \uput[-90](3,0){\color{DimGray} $y$}
    \uput[180](0,2){\color{DimGray} $z$}
    \psline{->}(0,0)(3;30)
    \psarc{->}(2.5;30){0.5}{100}{280}
    \psarc{->}(0,0){1.2}{0}{30}
    \uput{0.7}[15](0,0){\color{DimGray} $\varphi$}
    \uput{0.6}[180](2.5;30){\color{DimGray} $\theta$}
    \uput[30](3;30){\color{DimGray} $\hat{n}(\varphi)$}
  \end{pspicture}
  \caption*{Die Drehung $R_{\hat{n}(\phi)}(\theta)$}
\end{figure}
%
Es gilt:
%
\begin{align*}
  R_{\hat{n}(\phi)}(\theta) &= R_x(\phi) R_y(\theta) R_x(-\phi)
\end{align*}
%
Dabei bringt die Drehung $R_x(-\phi)$ die $\hat{n}(\phi)$-Achse auf die $y$-Achse, dann wird um die $y$-Achse mit dem Winkel $\theta$ gedreht und schließlich zur ursprünglichen Position der $\hat{n}(\phi)$-Achse mit $R_x(\phi)$ zurückgedreht.

Aus obiger Zeichnung wird mit der Vorstellung man >>multipliziert<< $T$ auf $\hat{n}(\phi)$ und auf die Achsen, so erhält man aus geometrischen Überlegungen:
%
\begin{align*}
  \bm{T}\hat{n}(\phi) &= \cos(\phi)T_y + \sin(\phi)T_z,
\end{align*}
%
wobei $\bm{T}\hat{y}= T_y$ und $T_z$ entsprechend gelte. Drückt man weiterhin $R_{\hat{n}(\phi)}(\theta)$ und $R_y(\theta)$ mit Hilfe des Stone Theorems \eqref{eq:Stone_Theorem} aus und entwickelt dann in $\theta$ bis zur ersten Ordnung, so ergibt sich:
%
\begin{align*}
  R_{\hat{n}(\phi)}(\theta) &= R_x(\phi) R_y(\theta) R_x(-\phi) \\
  \leadsto 1 - \mathrm{i} \theta \bm{T} \cdot \hat{n}(\phi) &= R_x(\phi) (1 - \mathrm{i} \theta T_y) R_x(-\phi) \\
  &= R_x(\phi) R_x(-\phi) - R_x(\phi) \mathrm{i} \theta T_y R_x(-\phi) \\
  &= 1 - \mathrm{e}^{-\mathrm{i}\phi T_x} \mathrm{i} \theta T_y \mathrm{e}^{\mathrm{i}\phi T_x} \\
  \implies \bm{T} \cdot \hat{n}(\phi) &= \mathrm{e}^{- \mathrm{i}\phi T_x} \mathrm{i} \theta T_y \mathrm{e}^{\mathrm{i}\phi T_x} \\
  \implies \cos(\phi) T_y + \sin(\phi) T_z &= \mathrm{e}^{-\mathrm{i}\phi T_x} \mathrm{i} \theta T_y \mathrm{e}^{\mathrm{i}\phi T_x}
\end{align*}
%
Dies vergleiche man mit der Relation aus Aufgabe 12a).

Entwickelt man nun noch beide Seiten bis zur ersten Ordnung in $\phi$, so ergibt sich:
%
\begin{align*}
  T_y + \phi T_z &= (1 - \mathrm{i} \phi T_x) T_y (1 + \mathrm{i} \phi T_x) \\
  &= T_y + \mathrm{i} \phi T_y T_x -\mathrm{i} \phi T_x T_y + \phi^2 T_x T_y T_x
\intertext{der letzte Term fliegt raus, da dieser quadratisch in $\phi$ ist}
  \leadsto T_z &= -\mathrm{i} (T_x T_y - T_y T_x) \\
  &= -\mathrm{i} [T_x,T_y] \\
  \implies [T_x,T_y] &= \mathrm{i} T_z
\end{align*}
%
Die anderen Kommutatorrelationen folgen durch zyklische Permutation.

Betrachten wir nun Operatoren, die Drehungen von physikalischen Zuständen im Hilbertraum $\mathcal{H}$ darstellen. Beim Stone Theorem haben wir gesehen, dass der Operator, der eine Drehung um einen Winkel $\theta$ um eine Achse $\hat{n}$ darstellt durch
%
\begin{align*}
  U_{\hat{n}}(\theta) &= \exp\left( -\mathrm{i} \theta \frac{\bm{J} \cdot \hat{n}}{\hbar} \right)
\end{align*}
%
gegeben ist. Diese Operatoren sind Repräsentanten der Drehgruppe, weswegen gefolgert werden kann, dass analog wie zuvor gilt:
%
\begin{align*}
  U[R_{\hat{n}(\phi)}(\theta)] &= U[R_x(\phi)] U[R_y(\theta)] U[R_x(-\phi)]
\end{align*}
%
Durch analoge Vorgehensweise wie oben können auch diese Kommutatorrelationen gefolgert werden:
%
\begin{align*}
  [J_x,J_y] &= \mathrm{i} \hbar J_z \\
  [J_y,J_z] &= \mathrm{i} \hbar J_x \\
  [J_z,J_x] &= \mathrm{i} \hbar J_y
\intertext{bzw.}
  [J_i,J_j] &= \mathrm{i} \hbar \sum\limits_{k} \varepsilon_{ijk} J_k
\end{align*}
%
Die Kommutatorrelationen der $J_i$ sind also bis auf einen Faktor $\hbar$ identisch mit denen für die $T_i$. In anderen Worte erfüllen die infinitesimalen Erzeuger von Drehungen in $\mathcal{H}$ dieselben Kommutatorrelationen wie die infinitesimalen Erzeuger der Drehgruppe im gewöhnlichen Raum.
