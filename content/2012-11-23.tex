% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 23.11.2012
%
\renewcommand{\printfile}{2012-11-23}

\subsection{Potentialtopf}
Als \acct{Potentialtopf} versteht man ein Potential, dass wie folgt dargestellt werden kann:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1)(1.5,1)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-1)(1.5,1)[\color{DimGray} $x$,0][\color{DimGray} $V(x)$,0]
    \psline(-0.5,-0.1)(-0.5,0.1)
    \psline(0.5,-0.1)(0.5,0.1)
    \uput[90](0.5,0){\color{DimGray} $a$}
    \uput[90](-0.5,0){\color{DimGray} $-a$}
    \uput[0](0.5,-0.5){\color{DarkOrange3} $-|V_0|$}
    \psline[linecolor=DarkOrange3](-1.3,0)(-0.5,0)(-0.5,-0.5)(0.5,-0.5)(0.5,0)(1.3,0)
  \end{pspicture}
\end{figure}

Offensichtlich existieren nur für:
\[
  0>E>-V_0 \qquad \text{wobei} \qquad V_0>0
\] 
gebundene Zustände. Für die Lösung der stationären Schrödingergleichung wird wieder in eine symmetrische und antisymmetrische Lösung unterschieden.
\begin{enum-roman}
  \item symmetrische Lösung:
  %
  \begin{item-triangle}
    \item $-a \leq x \leq a$:
    %
    \begin{align*}
      \phi(x) &= A \cos(qx) \qquad \text{mit} \qquad q = \sqrt{\frac{2m (|E|-V_0)}{\hbar^2}}
    \end{align*}
    
    \item $a<x$:
    %
    \begin{align*}
      \phi(x) &= B \exp\left(-kx\right) \qquad \text{mit} \qquad k = \sqrt{\frac{2m (-E)}{\hbar^2}}
    \end{align*}
  \end{item-triangle}
  %
  Für die Anschlussbedingung gilt:
  %
  \begin{item-triangle}
    \item Stetigkeit bei $x=a$:
    \[
      A \cos(qa) = B \exp\left(-ka\right)
    \]
    \item Symmetrie der Ableitung $\phi'(a+) = \phi'(a-)$:
    \[
      -qA\sin(qa) = -B\exp\left(-ka\right)
    \]
  \end{item-triangle}
  %
  Division der beiden Gleichungen miteinander liefert:
  %
  \begin{align*}
    \tan(qa) &= \frac{k}{q} \cdot {\color{DarkGray}\frac{a}{a}} \\
    &= \frac{\sqrt{\frac{2ma^2V_0}{\hbar^2}-(qa)^2}}{qa}
  \end{align*}
  %
  Grafisch beduetet dies:
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-0.5,-1)(5.5,2)
      \psaxes[labelFontSize=\scriptstyle\color{DimGray},trigLabelBase=2,dx=1,trigLabels,ticks=x,labels=x]{->}(0,0)(-0.5,-1)(5.5,2)[\color{DimGray} $qa$,0][\color{DimGray},0]
      \psplot[yMaxValue=2,yMinValue=-1,plotpoints=2000]{-0.5}{5}{0.2*tan(\psPiH*x)}
      \psplot[yMaxValue=2,yMinValue=0,plotpoints=2000,linecolor=DarkOrange3]{0.1}{2.9}{sqrt(3)/sqrt(x)-1}
      \psdot*[linecolor=DarkOrange3](0.86,0.85)
      \psdot*[linecolor=DarkOrange3](2.35,0.13)
      \uput[45](2.9,0){\color{DarkOrange3} $\sqrt{\frac{2 m a^2 V_0}{\hbar^2}}$}
      \psline[linecolor=DarkOrange3](2.9,-0.1)(2.9,0.1)
    \end{pspicture}
  \end{figure}
  %
  Es zeigt sich also, dass es mindestens eine Lösung gibt, im Allgemeinem aber endlich viele diskrete Energiewerte. Man spricht von Energiequantelung.
  
  Für den Grundzustand gilt:
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-2,-0.2)(2,1.5)
      \psaxes[labels=none,ticks=none]{->}(0,0)(-2,-0.2)(2,1.5)[\color{DimGray} $x$,0][\color{DimGray} $\phi(x)$,0]
      \psplot[linecolor=DarkOrange3]{-1}{1}{0.6*cos(\psPiH*x)+0.4}
      \psplot[linecolor=MidnightBlue]{1}{1.8}{0.4/x^2}
      \psplot[linecolor=MidnightBlue]{-1.8}{-1}{0.4/x^2}
      \psdots*(-1,0.4)(1,0.4)
    \end{pspicture}
  \end{figure}
  
  \item antisymmetrische Lösung:
  %
  \begin{item-triangle}
    \item $-a \leq x \leq a$:
    %
    \begin{align*}
      \phi(x) &= A \sin(qx) \qquad \text{mit} \qquad q = \sqrt{\frac{2m (|E|-V_0)}{\hbar^2}}
    \end{align*}
    
    \item $a<x$:
    %
    \begin{align*}
      \phi(x) &= B \exp\left(-kx\right) \qquad \text{mit} \qquad x = \sqrt{\frac{2m (-E)}{\hbar^2}}
    \end{align*} 
  \end{item-triangle}
  %
  Für die Anschlussbedingung gilt:
  %
  \begin{item-triangle}
    \item Stetigkeit bei $x=a$:
    \[
      A \sin(qa) = B \exp\left(-ka\right)
    \]
    \item Symmetrie der Ableitung $\phi'(a+) = \phi'(a-)$:
    \[
      -qA\cos(qa) = -B\exp\left(-ka\right)
    \]
  \end{item-triangle}
  %
  Division der beiden Gleichungen miteinander liefert:
  %
  \begin{align*}
    \tan(qa) &= -\frac{q}{k} \cdot {\color{DarkGray}\frac{a}{a}} \\
    &= \frac{qa}{\sqrt{\frac{2ma^2V_0}{\hbar^2}-(qa)^2}}
  \end{align*}
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-0.5,-2)(5.5,1)
      \psaxes[labelFontSize=\scriptstyle\color{DimGray},trigLabelBase=2,dx=1,trigLabels,ticks=x,labels=x]{->}(0,0)(-0.5,-2)(5.5,1)[\color{DimGray} $qa$,0][\color{DimGray},0]
      \psplot[yMaxValue=1,yMinValue=-2,plotpoints=2000]{-0.5}{5}{0.2*tan(\psPiH*x)}
      \psplot[yMaxValue=0,yMinValue=-2,plotpoints=2000,linecolor=DarkOrange3]{0.02}{2.9}{x/(x-3.8)}
      \uput[45](2.9,0){\color{DarkOrange3} $\sqrt{\frac{2 m a^2 V_0}{\hbar^2}}$}
      \psdot*[linecolor=DarkOrange3](1.25,-0.5)
      \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](2.9,0.1)(2.9,-2)
      \psline[linecolor=DarkOrange3](2.9,-0.1)(2.9,0.1)
    \end{pspicture}
  \end{figure}
\end{enum-roman}

\begin{notice*}
  Die grafische Lösung zeigt, dass die antisymmetrische Lösung nicht immer existieren muss!
\end{notice*}

\section{Symmetrie}

\subsection{Nichtentartung gebundener Zustände}

\begin{theorem*}
  In $d=1$ sind gebundene Zustände $\left(\lim\limits_{x \to \pm \infty} \phi(x) = 0\right)$ nicht entartet.
  \begin{proof}
    Der Satz wird durch einen Widerspruchsbeweis gezeigt:
    \begin{align*}
      - \frac{\hbar^2}{2m} \phi_1''(x) +V(x) \phi_1(x) &= E \phi_1(x) &&\big|\cdot \phi_2(x) \\
      - \frac{\hbar^2}{2m} \phi_2''(x) +V(x) \phi_2(x) &= E \phi_2(x) &&\big|\cdot \phi_1(x) \\
      \implies -\frac{\hbar^2}{2m} \left\{ \phi_1'' \phi_2 - \phi_2''\phi_1 \right\} &= 0 \\
      \implies \frac{\mathrm{d}}{\mathrm{d}x} \left\{ \phi_1' \phi_2 - \phi_2'\phi_1 \right\} &= 0 \\
      \implies  \phi_1' \phi_2 - \phi_2'\phi_1 &= \mathrm{const.} = 0
      \intertext{Dies ist der Fall, da nach Voraussetzung gilt: $\phi(x)\to 0$ und $\phi'(x) \to 0$. Weiter folgt also:}
      \frac{\phi_1'}{\phi_1} &= \frac{\phi'_2}{\phi_2} && \big| \int \; \mathrm{dx} \\
      \implies \ln(\phi_1) &= \ln(\phi_2) + \mathrm{const.}  && \big|\exp \\
      \phi_1(x) &= \exp(\mathrm{const.}) \cdot \phi_2(x)
    \end{align*}
    Mit der Normierung folgt:
    \[
    \phi_1(x) = \phi_2(x) 
    \]
    Dies stellt einen Widerspruch dar, da es zu zwei verschiedenen Wellenfunktionen keine identische Lösung geben kann.
  \end{proof}
\end{theorem*}

\begin{notice*}
  Wird mit den ebenen Wellen verglichen, so gilt:
  \[
    k = \pm \sqrt{\frac{2mE}{\hbar^2}}
  \]
  Die Wellenfunktion $\psi$ hat die Dimension:
  \[
    \psi = \frac{1}{\sqrt{\mathrm{Länge}}}
  \]
\end{notice*}

\subsection{Parität}

\begin{theorem*}
  Für ein Potential, dass die Bedingung:
  \[
    V(x) = V(-x)
  \]
  erfüllt, können die Eigenfunktionen des Hamiltonoperators $H$ symmetrisch, bzw. antisymmetrisch gewählt werden.
  
  \begin{proof}
    Sei $\phi(x)$ mit $E$ Lösung der stationären Schrödingergleichung, dann löst auch $\widetilde{\phi}(x)=\phi(-x)$ die stationäre Schrödingergleichung:
    \begin{align*}
      -\frac{\hbar^2}{2m} \widetilde{\phi}''(x)+V(x)\widetilde{\phi}(x) &= -\frac{\hbar^2}{2m} \widetilde{\phi}''(x) + V(-x)\widetilde{\phi}(x) \\ 
      &= -\frac{\hbar^2}{2m} \phi''(x) + V(-x) \phi(x) \\
      &= E \phi(-x) = E \widetilde{\phi}(x)
    \end{align*} 
    Somit löst auch
    \[
      \phi_{SG} \coloneq \phi(x) \pm \phi(-x)
    \]
    die stationäre Schrödingergleichung.
  \end{proof}
  
  \begin{proof}
    Zunächst definieren wir uns einen \acct{Paritäts-Operator} $\Pi$, der den Zustand an der Null spiegelt:
    %
    \begin{align*}
      \Pi \Ket{x} &= \Ket{-x} \\
      \Pi \Ket{\psi} &= \Pi \underbrace{\int \Ket{x}\Bra{x} \; \mathrm{d}x}_{\mathds{1}} \Ket{\psi} \\
      &= \int \Ket{x} \Braket{x|\psi} \; \mathrm{d}x \\
      &= \int \Ket{-x} \psi(x) \; \mathrm{d}x \\
      &= \int\limits_{-\infty}^{\infty} \psi(x) \Ket{-x} \; \mathrm{d}x 
    \end{align*}
    %
    Wird nun mit $y=-x$ substituiert, folgt:
    \[
      \Pi \Ket{\psi} = \int\limits_{-\infty}^{\infty} \psi(-y) \Ket{y} \; \mathrm{d}y
    \]
    Wird nun nochmals mit $y=x$ substituiert, ist das unerwünschte Minus im Ket eliminiert, und es folgt:
    \[
      \Pi \Ket{\psi} = \int\limits_{-\infty}^{\infty} \psi(-x') \Ket{x'} \; \mathrm{d}x'
    \]
    Somit folgt also:
    \[
      \implies \Braket{x|\Pi|\psi} = \psi(-x)
    \]
    Es folgt weiter:
    %
    \begin{align*}
      \Braket{x|\Pi|\psi} &= \Braket{-x|\psi} \\
      &= \frac{1}{\sqrt{2 \pi \hbar}} \exp\left(\mathrm{i}\frac{p}{\hbar} (-x) \right) \\
      &= \Braket{x|-p}
    \end{align*}
    %
    Somit folgt also auch:
    \[
      \pi \Ket{p} = \Ket{-p}
    \]
    Für die zweifache Anwendung des Paritäts Operators auf einen Zustand folgt somit:
    %
    \begin{align*}
      \Pi^2 \Ket{x} &= \Pi \Ket{-x} = \Ket{x} \\
      \implies \Pi^2 &= \mathds{1}  
    \end{align*}
    %
    Somit ist also $\Pi$ unitär und hermitesch. Weiterhin folgt, dass die Eigenwerte von $\Pi$ nur die Werte $\pm 1$ besitzen können.
    
    Die Eigenfunktionen von $\Pi$ haben somit eine eindeutige Parität:
    \[
      \Pi \Ket{\psi} = \pm \Ket{\psi}
    \]
    Das bedeutet für $\psi(x)$:
    \[
    \psi(x) = +\psi(-x) \qquad \text{oder} \qquad \psi(x) = -\psi(x)
    \]
    Wir nehmen an, das der Hamiltonoperator und der Paritäts-Operator miteinander kommutieren:
    \[
      [H,\Pi] = 0,
    \]
    dann gibt es eine gemeinsame Eigenbasis. Somit können auch die Eigenfunktionen von $H$ als symmetrisch, bzw. antisymmetrisch gewählt werden.
    \begin{notice*}[Frage:] Wann gilt $[H,\Pi]=0$?  \end{notice*}
    Der Hamiltonoperator kann auch geschrieben werden als:
    \[
      H = \frac{\hat{p}^2}{2m} + V(\hat{x})
    \]
    \begin{enum-arab}
      \item
      \begin{align*}
        [\hat{p}^2,\Pi] &= \hat{p}^2 \Pi - \Pi \hat{p}^2 &&\bigg|\Braket{p'|\ldots|p} \\
        \Braket{p'|[\ldots]|p} &= \left({p'}^2 - p^2\right) \Braket{p'|\Pi|p} \\
        &= \left({p'}^2 - p^2\right) \Braket{p'|-p} \\
        &= \left({p'}^2 - p^2\right) \delta\left(p'+p\right) \\
        &= 0 &&\text{immer!}
      \end{align*}
      Physikalisch bedeutet dies, wenn die kinetische Energie mit $\Pi$ vertauscht, dann besitzen $\Pi$ und $p^2$ nach der v.S.k.O eine Eigenbasis.
      \begin{notice*}
        \[
          [\hat{p},\Pi] = \left(p'-p\right) \delta\left(p'+ p\right) \neq 0
        \]
      \end{notice*}
      \item
      \begin{align*}
        [V(\hat{x})] &= V(\hat{x}) \Pi - \Pi V(\hat{x}) &&\bigg|\Braket{x'|\ldots|x} \\
        \Braket{x'|[\ldots]|x} &= \left(V(\hat{x}')-V(\hat{x})\right) \Braket{x'|\Pi|x} \\
        &= \left(V(\hat{x}')-V(\hat{x})\right) \Braket{x'|-x} \\
        &= \left(V(\hat{x}')-V(\hat{x})\right) \delta\left(x'+x\right) \\
        &= 0 \quad \iff \quad V(x) = V(-x)
      \end{align*}
    \end{enum-arab}
  \end{proof}
\end{theorem*}

\begin{notice*}
  Für ebene Wellen gilt:
  %
  \begin{align*}
    &\exp(\mathrm{i}kx) &\text{rechtslaufend} \\
    &\exp(-\mathrm{i}kx) &\text{linkslaufend} \\
  \end{align*}
  %
  Dies sind Eigenfunktionen des Impulsoperators, nicht aber des Paritäts-Operators
\end{notice*}
