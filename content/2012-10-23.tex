% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 23.10.2012
%
\renewcommand{\printfile}{2012-10-23}

\subsection{Projektionsoperator}

$\mathcal{H}_1$ und $\mathcal{H}_2$ sind orthogonale Unterräume des Hilbertraumes $\mathcal{H}$ so, dass 
%
\begin{gather*}
  \forall \, \Ket{\psi} \in \mathcal{H} \, \exists \, \Ket{\psi_1} \in \mathcal{H}_1, \Ket{\psi_2} \in \mathcal{H}_2 \; : \; \Ket{\psi} = \Ket{\psi_1} + \Ket{\psi_2}
\end{gather*}
%
(Direkte Summe $\mathcal{H} = \mathcal{H}_1 \oplus \mathcal{H}_2$)

\begin{theorem*}[Definition] Projektionsoperator
  \begin{align*}
    P_1\Ket{\psi} = \Ket{\psi_1} \; , \quad \forall \Ket{\psi} \in \mathcal{H}
  \end{align*}
  %
  Mit den Eigenschaften
  \begin{item-triangle}
    \item $P_1$ linear
    
    \item $P_1$ hermitesch
    \begin{align*}
      \Braket{\phi|P_1^\dagger|\psi} &= \Braket{P_1 \phi|\psi} \\
      &= \Braket{\phi_1|\psi} \\
      &= \Braket{\phi_1|\psi_1} \\
      &= \Braket{\phi|\psi_1} \\
      &= \Braket{\phi|P|\psi} \\
      \implies P &= P^ \dagger
    \end{align*}
    
    \item Der Operator $P_1$ ist zudem indempotent,
    %
    \begin{align*}
      (P_1)^2 = P_1
    \end{align*}
  \end{item-triangle}
\end{theorem*}

Mit Projektion auf einen Basisvektor $\Ket{n}$ erhält man
%
\begin{align*}
  P_n &= \Ket{n} \Bra{n} \\
  P_n \Ket{\psi} &= \Ket{n} \Braket{n|\psi} = c_n \Ket{n} .
\end{align*}

\begin{notice*}[Wichtig:]
  Zerlegung der $1$ (Resolution of unity)
  %
  \begin{align*}
    \mathds{1} &= \sum\limits_{n} \Ket{n}\Bra{n} \\
    \mathds{1} \Ket{\psi} &= \sum\limits_{n} \Ket{n}\Braket{n|\psi} = \sum\limits_{n} c_n \Ket{n}
  \end{align*}
\end{notice*}
%
Projektion auf einen beliebigen Vektor $\Ket{\phi}$
%
\begin{align*}
  P_\phi = \Ket{\phi} \Bra{\phi}
\end{align*}
%
Matrixdarstellung
%
\begin{align*}
  P_n = \Ket{n}\Bra{n}=
  \left(
  \begin{matrix}
    0 \\
    \vdots \\
    1 \\
    \vdots \\
    0
  \end{matrix}
  \right)
  \cdot (0,0,\ldots,1,\ldots,0) =
  \left(
  \begin{matrix}
    0 & \ldots & \ldots & \ldots & 0 \\
    \vdots & \ddots & \ddots & \ddots & \vdots \\
    \vdots & \ddots & 1 & \ddots & \vdots \\
    \vdots & \ddots & \ddots & \ddots & \vdots \\
    0 & \ldots & \ldots & \ldots & 0 \\
  \end{matrix}
  \right)
\end{align*}
%
Wobei die $1$ in der Matrix an der Stelle $A_{nn}$ steht.
%
\begin{align*}
\sum\limits_{n} \Ket{n} \Bra{n} \hateq \mathds{1} = \begin{pmatrix}
1 & \ldots & 0 \\
\vdots & \ddots & \vdots \\
0 & \ldots & 1 \\
\end{pmatrix}
\end{align*}

\subsection{Unitäre Operatoren}

\index{Unitärer Operator}

\begin{theorem*}[Definition]
  \begin{align*}
    U \text{ unitär} \iff U^\dagger U = \mathds{1} = U U^\dagger \iff U^\dagger = U^{-1}
  \end{align*}
\end{theorem*}

\begin{theorem*}[Satz]
Ist $U$ unitär so folgt für die Norm:
%
\begin{align*}
  \|U \phi\| &= \|\phi\|
\end{align*}
\end{theorem*}

\begin{proof}
  \begin{enum-arab}
    \item >>$\implies$<<
    %
    \begin{align*}
      \|U\phi\|^2 &= \Braket{U\phi|U \phi} = \braket{\phi|\underbrace{U^\dagger U}_{\mathds{1}}|\phi} = \Braket{\phi|\phi} = \|\phi\|^2
    \end{align*}
    
    \item >>$\impliedby$<<
    \begin{align}
      \|U (\phi + c \chi)\|^2 &= \Braket{U \phi|U \phi} + c c^* \Braket{U \chi|U \chi} + 2 \Re (c\Braket{U \phi|U \chi}) \tag{1} \label{eq:1} \\
      \|\phi + c \chi\|^2 &= \Braket{\phi|\phi} + c c^* \Braket{\chi|\chi} + 2 \Re (c\Braket{\phi|\chi}) \tag{2}\label{eq:2}
    \end{align}
    %
    Dies folgt aus der Erhaltung der Norm $\Braket{U \phi|U \phi} \equiv \Braket{\phi|\phi}$
    
    \eqref{eq:1} = \eqref{eq:2}
    %
    \begin{align*}
      \Re (c\Braket{U\phi|U\chi}) = \Re (c\Braket{\phi|\chi})
    \end{align*}
    %
    Unterscheide die Fälle
    \begin{item-triangle}
      \item $c = 1 \implies \Re(\Braket{U\phi|U\chi}) = \Re(\Braket{\phi|\chi})$
      
      \item $c = \mathrm{i} \implies \Im(\Braket{U\phi|U\chi}) = \Im(\Braket{\phi|\chi})$
    \end{item-triangle}
    %
    \begin{align*}
      \implies \Braket{\phi|\chi} &= \Braket{U\phi|U\chi} = \braket{\phi|\underbrace{U^\dagger U}_{=1}|\chi} = \Braket{\phi|\chi}
    \end{align*}
  \end{enum-arab}
\end{proof}

\begin{notice*}
  $U$ ist \acct{Isometrie}.
\end{notice*}

Unitäre Operatoren vermitteln Wechsel zwischen Orthonormalbasen. Sei 
%
\begin{align*}
  \mathcal{B}_1 = \left\{ \Ket{n}  \right\} \; , \quad \mathcal{B}_2 = \left\{ \Ket{n'} : \Ket{n'} = U \Ket{n} \right\}
\end{align*}
%
damit
%
\begin{align*}
  \Braket{m'|n'} &= \Braket{U \, m | U \, n} = \Braket{m|U^\dagger U|n} = \Braket{m|n} = \delta_{nm}
\end{align*}

\begin{enum-arab}
  \item
  \begin{align*}
    \Ket{\psi} &= \sum\limits_{n} c_n \Ket{n} = \sum\limits_{n'} c_n' \Ket{n'}
  \intertext{mit}
    c_n' &= \Braket{n'|\psi} = \Braket{Un|\psi} = \Braket{n|U^\dagger|\psi} \\
    &= \sum\limits_{m} \Braket{n|U^\dagger|m} \Braket{m|\psi} \\
    &= \sum\limits_{m} U_{nm}^\dagger \, c_m
  \end{align*}
  
  \item Matrixelemente:
  %
  \begin{align*}
    A_{mn}' = \Braket{m'|A|n'} = \sum\limits_{k,\ell} U_{mk}^\dagger A_{k\ell} U_{\ell n}
  \end{align*}
\end{enum-arab}

\subsection{Spektralzerlegung von hermiteschen Operatoren}

\begin{theorem*}[Satz]
  Ist $A$ \acct[0]{hermitesch}, so sind alle seine Eigenwerte $a_n$ reell und alle Eigenvektoren zu unterschiedlichen Eigenwerten orthogonal zueinander.
  
  \begin{proof}
    $A\Ket{a_n} = a_n\Ket{a_n}$
    %
    \begin{enum-arab}
      \item
      \begin{align*}
        \Braket{a_n|A|a_n} &= \Braket{a_n|A \, a_n} = a_n \Braket{a_n|a_n} = \Braket{A \, a_n| a_n} = a_n^* \Braket{a_n|a_n} \\
        a_n &= a_n^* \quad \text{also } a_n \in \mathbb{R}
      \end{align*}
      
      \item
      \begin{gather*}
        \begin{aligned}
          A\Ket{a_n} = a_n\Ket{a_n} \\
          A\Ket{a_m} = a_m\Ket{a_m}
        \end{aligned} \tag{2} \\
        \begin{aligned}
          \Braket{a_m|A|a_n} &= a_n \Braket{a_m|a_n} \notag \\
                             &= a_m \Braket{a_m|a_n} \notag \\
        \end{aligned} \notag \\
        \implies \Braket{a_m|a_n} = 0 \qquad \text{für $a_m \neq a_n$}
      \end{gather*}
    \end{enum-arab}
  \end{proof}
\end{theorem*}

\begin{notice*}[Folgerungen]
  \begin{item-triangle}
    \item Falls alle $a_n$ unterschiedlich sind, dann \[ \left\{\Ket{a_n} \right\} \coloneq b \] bilden eine Basis von $\mathcal{H}$
    
    \item Falls nicht alle $a_n$ unterschiedlich sind, dann lässt sich aus den $\Ket{a_n}$ eine Orthonormalbasis konstruieren.
  \end{item-triangle}
\end{notice*}

Sei $g(n)$ die Entartung (Vielfachheit) des Eigenwertes $a_n$, dann gibt es im Eigenraum $E_n \subset \mathcal{H}$ eine Orthonormalbasis aus $g(n)$ Vektoren \[ \Ket{n,r} \quad (r=1,2,\ldots ,g(n))\] mit \[A\Ket{n,r} = a_n \Ket{n,r}.\]

Eine Projektion auf diesen Unterraum $E_n$ verläuft dann
%
\begin{align*}
  P_n &= \sum\limits_{n=1}^{g(n)} \Ket{n,r} \Bra{n,r} \\
  \sum\limits_{n} P_n &= \sum\limits_{n=1}^{K} \sum\limits_{n=1}^{g(n)} \Ket{n,r} \Bra{n,r} = \mathds{1}
\end{align*}
%
mit $K = N - \sum\limits_{m} g(m)$.

\subsection{Spektraldarstellung}

\begin{theorem*}[Defintion] \acct{Spektraldarstellung}
  \begin{align*}
    A &= A \cdot \mathds{1} \\
    &= \sum\limits_{n}^{N} \sum\limits_{r=1}^{g(n)} A \Ket{n,r} \Bra{n,r} \\
    &= \sum\limits_{n}^{N} \sum\limits_{r=1}^{g(n)} a_n \Ket{n,r} \Bra{n,r}
  \end{align*}
  %
  ohne Entartung
  %
  \begin{align*}
    \mathds{1} &= \sum\limits_{n} \Ket{n} \Bra{n} \\
    A \cdot \mathds{1} &= \sum\limits_{n}^{N} A \Ket{n} \Bra{n} \\
    &= \sum\limits_{n}^{N} a_n \Ket{n} \Bra{n}
  \end{align*}
\end{theorem*}

\subsection{Vollständiger Satz kommutierender Operatoren (VSKO)}

\begin{theorem*}[Defintion]
  $A$,$B$ kommutieren $\iff [A,B] = 0 = AB - BA$
\end{theorem*}

\begin{theorem*}[Satz]
   Seien $A$ und $B$ hermitesche Operatoren mit der Eigenschaft $[A,B]=0$ so existiert eine gemeinsame Eigenbasis von $A$ und $B$.
  
  \begin{proof}
    Sei $A\Ket{n,r} = a_n \Ket{n,r}$
    %
    \begin{align*}
      B \, A \Ket{n,r} &= a_n \, B \Ket{n,r} \\
      A \, B \Ket{n,r} &= A \underbrace{[B \Ket{n,r}]}_{\Ket{\phi}} = a_n \Ket{\phi}
    \end{align*}
    %
    $\Ket{\phi} = B \Ket{n,r}$ liegt im Eigenraum $\varepsilon_n$ zum Eigenwert $a_n$.
    
    \begin{enum-arab}
      \item Keine Entartung: $g(n) = 1$
      %
      \begin{align*}
        B \Ket{n} = b_n \Ket{n}
      \end{align*}
      %
      mit $b_n \in \mathbb{R}$
          
      \item Mit Entartung: $g(n) > 1$
      %
      \begin{align*}
        \implies \Braket{m,s|B|n,r} = \delta_{mn} B_{sr}^{(m)}
      \end{align*}
    \end{enum-arab}
    %
    Matrixdarstellung von B in einer beliebigen Eigenbasis von A:
    %
    \begin{align*}
      B =
      \begin{pmatrix}
        B^{(1)} & 0       & 0 \\
        0       & B^{(2)} & 0 \\
        0       & 0       & B^{(n)} 
      \end{pmatrix}
    \end{align*}
    %
    $B$ kann also diagonalisiert werden ohne die Eigenbasis von A zu verlassen. Falls $B^{(n)}$ noch entartet sind, sucht man einen Operator $C$, der mit $A$ und $B$ kommutiert.
  \end{proof}
\end{theorem*}

\begin{theorem*}[Defintion]
  Das \acct[0]{Ensemble} $\left\{ A^1, \ldots , A^m \right\}$ wechselseitig kommutierender Operatoren, deren Eigenwerte $(a_{k1}^1, \ldots , a_{nm}^m)$ und Eigenvektoren $\Ket{a_{n1}^1 \ldots a_{nm}^m}$ mit
  %
  \begin{align*}
    A^k \Ket{a_{k1}^1 \ldots a_{km}^m} = a_{nk}^k \Ket{a_{k1}^1 \ldots a_{km}^m}
  \end{align*}
  %
  eine eindeutige Basis definieren, heißt \acct{vollständiger Satz kommutierender Operatoren}.
\end{theorem*}

% % % BEISPIEL VSKO

\begin{example*}

Gegeben seien die Matrizen
%
\begin{align*}
  A &= K
  \begin{pmatrix}
    2  & -1 & -1 \\
    -1 & 2  & -1 \\
    -1 & -1 &  2
  \end{pmatrix}
  \\
  B &= \mathrm{i} J
  \begin{pmatrix}
    0  & 1 & -1 \\
    -1 & 0  & 1 \\
    1 & -1 &  0
  \end{pmatrix}
\end{align*}
mit $K,J \in \mathbb{R}$.

\minisec{Vorgehensweise}

Berechne zuerst alle Eigenwerte und die dazugehörigen Eigenvektoren von $A$. Eigenwerte:
%
\begin{align*}
  a_1 = 0 \qquad a_2 = 3K \qquad \text{mit Entartung } g(1)=1 \; , \quad g(2) = 2
\end{align*}
%
Normierter Eigenvektor zum Eigenwert $a_1$:
%
\begin{align*}
  \Ket{a_1} = \frac{1}{\sqrt{3}} \begin{pmatrix} 1 \\ 1 \\ 1 \end{pmatrix}
\end{align*}
%
Der Eigenraum $\mathcal{E}_2$ zum Eigenwert $a_2$ ist zweidimensional. Innerhalb dieses Eigenraumes gibt es unendlich viele Orthonormalbasen $\mathcal{B}_n$ eine davon ist
%
\begin{align*}
  \mathcal{B}_1 = \left\{
  \Ket{a_2^{(1)},1} = \frac{1}{\sqrt{6}}
  \begin{pmatrix} -2 \\ 1 \\ 1 \end{pmatrix}
  \; , \;
  \Ket{a_2^{(1)},2} = \frac{1}{\sqrt{2}}
  \begin{pmatrix} 0 \\ -1 \\ 1 \end{pmatrix}
  \right\} ,
\end{align*}
%
eine weitere ist
%
\begin{align*}
  \mathcal{B}_2 = \left\{
  \Ket{a_2^{(1)},1} = \frac{1}{\sqrt{2}}
  \begin{pmatrix} -1 \\ 0 \\ 1 \end{pmatrix}
  \; , \;
  \Ket{a_2^{(1)},2} = \frac{1}{\sqrt{2}}
  \begin{pmatrix} -1 \\ 1 \\ 0 \end{pmatrix}
  \right\} .
\end{align*}

\minisec{Problem}

Durch die Angabe des Eigenwertes $a_2$ ist kein eindeutiger Eigenvektor $\Ket{a_2} \in \mathcal{H}$ festgelegt. Wir benötigen nun einen hermiteschen Operator der mit $A$ kommutiert. In unserem Fall erfüllt $B$ diese Voraussetzungen. \[[A,B]=0 \] Der unitäre Operator $U$, welcher aus den Eigenvektoren von $A$ ($\mathcal{B}_1$) gebastelt wird lautet
%
\begin{align*}
  U =
  \begin{pmatrix}
  \frac{1}{\sqrt{3}} & -\frac{\sqrt{2}}{\sqrt{3}}  & 0 \\
  \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{6}} & -\frac{1}{\sqrt{2}} \\
  \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{6}} & \frac{1}{\sqrt{2}}
  \end{pmatrix} .
\end{align*}
%
Der Operator $U$ vermittelt den Übergang von der Basis $\mathcal{B}$ zur Basis $\mathcal{B}_1$. Operator $B$ ist in der Basis $\mathcal{B}_1$ blockdiagonal.
%
\begin{align*}
  U^\top B U =
  J
  \begin{pmatrix}
    0 & 0 & 0 \\
    0 & 0 & \mathrm{i}\sqrt{3} \\
    0 & -\mathrm{i}\sqrt{3} & 0
  \end{pmatrix}
\end{align*}
%
B kann durch Diagonalisierung der Diagonalblöcke als ganzes diagonalisiert werden. Die Eigenwerte sind dann:
%
\begin{align*}
  b_1 = 0 \qquad b_2 = J \sqrt{3} \qquad b_3 = J -\sqrt{3}
\end{align*}
%
Es liegt nun keine Entartung der Eigenwerte mehr vor. Die Eigenvektoren zu den zugehörigen Eigenwerten in der Basis $\mathcal{B}_1$ lauten wie folgt
%
\begin{align*}
  \Ket{b_1}_{\mathcal{B}_1} &= \begin{pmatrix} 1 \\ 0 \\ 0 \end{pmatrix} \\ 
  \Ket{b_2}_{\mathcal{B}_1} &= \frac{1}{\sqrt{2}} \begin{pmatrix} 0 \\ -\mathrm{i} \\ 1 \end{pmatrix} \\
  \Ket{b_2}_{\mathcal{B}_1} &= \frac{1}{\sqrt{2}} \begin{pmatrix} 0 \\ \mathrm{i} \\ 1 \end{pmatrix}
\end{align*}
%
In der Basis $\mathcal{B}$ lauten die Eigenvektoren
%
\begin{align*}
  \Ket{b_1}_{\mathcal{B}} &= \Ket{a_1} \\
  \Ket{b_2}_{\mathcal{B}} &= \frac{1}{\sqrt{2}} \left( \Ket{a_2^{(1)},2} - i\Ket{a_2^{(1)},1} \right) \\
  \Ket{b_2}_{\mathcal{B}} &= \frac{1}{\sqrt{2}} \left( \Ket{a_2^{(1)},2} + i\Ket{a_2^{(1)},1} \right)
\end{align*}
%
Durch $\left\{\Ket{b_2}, \Ket{b_3}\right\}$ ist eine durch $B$ ausgezeichnete Basis des Unterraums $\mathcal{E}_2$ gegeben.

Durch die Angabe des Pärchens $(a_j,b_j)$ mit $j=1,2,3$ ist ein eindeutiger Einheitsvektor $\Ket{a_j,b_j}$ festgelegt.

$\left\{ (a_j,b_j)  \right\}_{j=1,2,3}$ ist eine Orthonormalbasis von $\mathcal{H}$ und $\left\{ A,B \right\}$ ist ein vollständiger Satz kommutierender Operatoren.

Es gilt:
%
\begin{align*}
  A\Ket{a_j,b_j} &= a_j \Ket{a_j,b_j} \\
  B\Ket{a_j,b_j} &= b_j \Ket{a_j,b_j} \\
  \Ket{a_1,b_1} &= \Ket{a_1} \\
  \Ket{a_2,b_2} &= \Ket{b_2} \\
  \Ket{a_3,b_3} &= \Ket{b_3}
\end{align*}
%
~ % Dirty endsymbol fix
\end{example*}
