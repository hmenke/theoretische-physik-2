% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung vom 20.11.2012
%
\renewcommand{\printfile}{2012-11-20}

\section{Potentialstufen und -töpfe}

\subsection{Wahrscheinlichkeitsstrom}

\begin{align*}
  \varrho(x,t) &= \psi(x,t) \psi^*(x,t)
\end{align*}
%
ist die Wahrscheinlichkeit ein Teilchen bei einer Ortsmessung an der Position x zu finden.
%
\begin{align*}
  \partial_t \varrho(x,t)
  &= (\partial_t \psi) \psi^* + \psi (\partial_t \psi^*) \\
  &= \left( - \frac{\mathrm{i}}{\hbar} H \psi \right) \psi^* + \psi \left( - \frac{\mathrm{i}}{\hbar} H \psi^* \right) \\
  &= - \frac{\mathrm{i}}{\hbar} \left[ \left( - \frac{\hbar^2}{2m} \partial_x^2 + V(x) \right) \psi \right] + \psi \frac{\mathrm{i}}{\hbar} \left[ \left( - \frac{\hbar^2}{2m} \partial_x^2 + V(x) \right) \psi^* \right] \\
  &= \frac{\mathrm{i} \hbar}{2 m} \partial_x \left[ (\partial_x \psi) \psi^* - \psi (\partial_x \psi^*) \right] \\
  &\eqcolon - \partial_x j(x,t) \qquad \text{\acct{Wahrscheinlichkeitsstrom}}
\end{align*}
%
mit
%
\begin{align*}
  j(x,t)
  &= - \frac{\mathrm{i} \hbar}{2 m} \left[ \psi' \psi^* - \psi {\psi^*}' \right] \\
  &= \frac{\hbar}{m} \Im (\psi' \psi^*)
\end{align*}
%
\begin{notice*}
  Analog zur Kontinuitätsgleichung aus der Elektrodynamik $\div \bm{j} = - \partial_t \varrho$.
\end{notice*}
%
\begin{example*}
  Ebene Welle
  %
  \begin{align*}
    \psi(x,t) &= \frac{1}{\sqrt{2 \pi \hbar}} \exp\left( \mathrm{i} k x - \omega t \right) \\
    j(x,t) &= \frac{\hbar}{m} \frac{1}{2 \pi \hbar} \Im (\mathrm{i} k) \\
    &= \frac{1}{2 \pi \hbar} \frac{\hbar k}{m}
  \end{align*}
\end{example*}

\subsection{Streueung an der Potentialstufe} \index{Potentialstufe}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-0.2)(2,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-2,-0.2)(2,1.5)[\color{DimGray} $x$,0][\color{DimGray} $V(x)$,0]
    \psline[linecolor=DarkOrange3](-2,0)(0,0)(0,1)(2,1)
    \uput[180](0,1){\color{DarkOrange3} $V_0$}
  \end{pspicture}
\end{figure}
%
Für die Potentialstufe gilt folgendes Potential:
%
\begin{align*}
  V(x) = 
  \begin{cases}
    V_0 & x \geq 0 \\
    0 & x < 0
  \end{cases}
\end{align*}
%
Wir behandeln das Problem als stationäres Problem und betrachten die einfallende Welle als ebene Welle unendlicher Ausdehnung. Stationäre Schrödingergleichung:
%
\begin{align*}
  \left[ - \frac{\hbar^2}{2 m} \partial_x + V(x) \right] \phi(x) = E \phi(x)
\end{align*}

\minisec{Fall 1: $E > V_0$}

Für $x < 0$ lautet die Lösung
%
\begin{align*}
  \phi(x) = A \exp(\mathrm{i}kx) + B \exp(-\mathrm{i}kx) \; , \quad \text{mit } k = \sqrt{\frac{2 m E}{\hbar^2}}
\end{align*}
%
Für $x > 0$ lautet die Lösung
%
\begin{align*}
  \phi(x) = C \exp(\mathrm{i}qx) + D \exp(-\mathrm{i}qx) \; , \quad \text{mit } q = \sqrt{\frac{2 m (E-V_0)}{\hbar^2}}
\end{align*}
%
Randbedingungen bei $x = 0$
%
\begin{align*}
  \left[ - \frac{\hbar^2}{2 m} \partial_x + V(x) \right] \phi(x) &= E \phi(x) && \bigg| \int\limits_{-\varepsilon}^{\varepsilon} \mathrm{d}x \\
  -\frac{\hbar^2}{2 m} \left( \phi'(+\varepsilon) - \phi'(-\varepsilon) \right) + \mathcal{O}(\varepsilon) = E \, \mathcal{O}(\varepsilon)
\end{align*}
%
Also muss $\left( \phi'(+\varepsilon) = \phi'(-\varepsilon) \right)$, folglich ist $\phi'$ stetig. Aus der Stetigkeit folgt weiterhin
%
\begin{enum-arab}
  \item $\phi$ stetig
  %
  \begin{align*}
    \phi(+0) &= \phi(-0) \\
    A + B &= C + D
  \end{align*}
  
  \item $\phi'$ stetig
  %
  \begin{align*}
    \phi'(+0) &= \phi'(-0) \\
    \mathrm{i} k (A-B) &= \mathrm{i} q (C-D)
  \end{align*}
\end{enum-arab}
%
Auslösen der Gleichungen liefert
%
\begin{align*}
  \begin{pmatrix} A \\ B \end{pmatrix}
  &=
  \frac{1}{2 k}
  \begin{pmatrix}
    k + q & k - q \\
    k - q & k + q \\
  \end{pmatrix}
  \begin{pmatrix} C \\ D \end{pmatrix}
\end{align*}
%
Weitere Randbedingungen: Die Welle fällt von links ein. Also ist
%
\begin{align*}
  D = 0
\end{align*}
%
Wähle $A = 1$, damit folgt für die anderen Konstanten
%
\begin{align*}
  B = \frac{k - q}{k + q} \qquad C = \frac{2 k}{k + q}
\end{align*}
%
Betrachte den Wahrscheinlichkeitsstrom für $x > 0$:
%
\begin{align*}
  j(x,0)
  &= \frac{\hbar}{m} \Im(\phi' \phi^*) \\
  &= \frac{\hbar}{m} \Im(C \exp(\mathrm{i}qx) \, C^* \exp(-\mathrm{i}qx)) \\
  &= \frac{\hbar}{m} C C^* q \\
  &= \frac{\hbar}{m} \left(\frac{2 k}{k + q}\right)^2 q \\
  &\eqcolon j_T \qquad \text{\acct[0]{Transmittierter Strom}\index{Wahrscheinlichkeitsstrom!transmittiert}}
\end{align*}
%
Der Wahrscheinlichkeitsstrom für $x < 0$ ist
%
\begin{align*}
  j(x,0)
  &= \frac{\hbar}{m} \Im\Big[ \mathrm{i}k \left( A \exp(\mathrm{i}kx) - B \exp(\mathrm{i}kx) \right) \left( A \exp(-\mathrm{i}kx) + B \exp(\mathrm{i}kx) \right) \Big] \\
  &= \frac{\hbar}{m} \Im\Big[ \mathrm{i}k \left( A^2 - B^2 \right) \Big] \\
  &= \frac{\hbar}{m} k \left( A^2 - B^2 \right) \\
  &= \frac{\hbar}{m} k \left( 1 - \frac{(k - q)^2}{(k + q)^2} \right) \\
  &= \frac{\hbar}{m} k - \frac{\hbar}{m} k \left( \frac{k - q}{k + q} \right)^2  \\
  &\eqcolon j_I - j_R
\end{align*}
%
$j_I$ ist der \acct[0]{einfallende Wahrscheinlichkeitsstrom}.\index{Wahrscheinlichkeitsstrom!einfallend}

$j_R$ ist der \acct[0]{reflektierte Wahrscheinlichkeitsstrom}.\index{Wahrscheinlichkeitsstrom!reflektiert}

Wir definieren
%
\begin{align*}
  j_R &= \frac{\hbar}{m} k \left( \frac{k - q}{k + q} \right)^2 = j_I \, R && R : \text{\acct{Reflexionskoeffizient}} \\
  j_T &= \frac{\hbar}{m} q \left(\frac{2 k}{k + q}\right)^2 = j_I \, T && T : \text{\acct{Transmissionskoeffizient}}
\end{align*}
%
Also
%
\begin{align*}
  R \coloneq \left( \frac{k - q}{k + q} \right)^2 \qquad T \coloneq \left(\frac{2 k}{k + q}\right)^2 \frac{q}{k}
\end{align*}
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-1)(3,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.5,-0.5)(3,1.5)[\color{DimGray} $q$,0][\color{DimGray},0]
    \psaxes[labels=none,ticks=none,yAxis=false]{->}(0,-1)(-0.5,0)(3,0)[\color{DimGray} $E$,0][\color{DimGray},0]
    \psplot[plotpoints=400,linecolor=MidnightBlue]{0}{1.5}{1-2.718281828459045^(-3*x)}
    \psplot[plotpoints=400,linecolor=DarkOrange3]{0}{1.5}{2.718281828459045^(-3*x)}
    \psline(1.5,-0.1)(1.5,0.1)
    \psline(0,-1.1)(0,-0.9)
    \psline(1.5,-1.1)(1.5,-0.9)
    \psline[linestyle=dotted,dotsep=1pt](0,1)(1.5,1)
    \uput[180](0,1){\color{DimGray} $1$}
    \uput[45](1.5,0){\color{DarkOrange3} $R$}
    \uput[0](1.5,1){\color{MidnightBlue} $T$}
    \uput[-90](1.5,0){\color{DimGray} $k$}
    \uput[-90](0,-1){\color{DimGray} $V_0$}
    \uput[-90](1.5,-1){\color{DimGray} $\infty$}
  \end{pspicture}
\end{figure}
%
Außerdem muss gelten
%
\begin{align*}
  R + T = 1
\end{align*}
%
Selbst für $E > V_0$ wird ein Teil reflektiert!

\minisec{Fall 2: $0 < E < V_0$}

Für $x < 0$ lautet die Lösung (mit $A = 1$)
% 
\begin{align*}
  \phi(x) = \exp(\mathrm{i}kx) + B \exp(-\mathrm{i}kx) \; , \quad \text{mit } k = \sqrt{\frac{2 m E}{\hbar^2}}
\end{align*}
%
Für $x > 0$ lautet die Lösung
%
\begin{align*}
  - \frac{\hbar^2}{2 m} \partial_x^2 \phi(x) &= \underbrace{(E-V_0)}_{<0} \phi(x) \\
  \phi(x) &= C \exp(-\kappa x) + D \exp(\kappa x) \; , \quad \text{mit } \kappa = \sqrt{\frac{2 m (V_0-E)}{\hbar^2}}
\end{align*}
%
Wähle $D = 0$ wegen Endlichkeit, das heißt, dass die Welle im unendlichen verschwinden muss. Mit Stetigkeit bei $x = 0$ folgt
%
\begin{align*}
  1 + B = C
\end{align*}
%
Aus der Differenzierbarkeit erhalten wir
%
\begin{align*}
  \mathrm{i} k (1 - B) = - \kappa C
\end{align*}
%
Also lauten unsere Konstanten
%
\begin{align*}
  C = \frac{2 k}{k + \mathrm{i} \kappa}, \qquad B = \frac{\kappa+ \mathrm{i} k}{\mathrm{i} k - \kappa}
\end{align*}
%
Damit lautet die Wellenfunktion
%
\begin{align*}
  \phi(x) =
  \begin{dcases}
    \exp(\mathrm{i}kx) + \left( \frac{k - \mathrm{i} \kappa}{k + \mathrm{i} \kappa} \right) \exp(-\mathrm{i}kx) & \text{für } x < 0 \\
    \frac{2 k}{k + \mathrm{i} \kappa} \exp(-\kappa x) & \text{für } x > 0
  \end{dcases}
\end{align*}
%
Betrachte den Wahrscheinlichkeitsstrom:
%
\begin{align*}
  \varrho(x) = |\phi(x)|^2 = \frac{4 k^2}{k^2 + \kappa^2} \exp(-2 \kappa x)
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-0.2)(2,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-2,-0.2)(2,1.5)[\color{DimGray} $x$,0][\color{DimGray} $V(x)$,0]
    \psline[linecolor=DarkOrange3](-2,0)(0,0)(0,1)(2,1)
    \uput[180](0,1){\color{DarkOrange3} $V_0$}
    \psplot[plotpoints=400,linecolor=MidnightBlue]{0}{1.5}{1.3*2.718281828459045^(-3*x)}
    \uput[0](1,0.5){\color{MidnightBlue} $\varrho(x)$}
  \end{pspicture}
\end{figure}
%
\begin{align*}
  j_T
  &= \frac{\hbar}{m} \Im \Big( \phi \phi^* \Big) \\
  &= 0
\end{align*}
%
Somit ist der Transmissionskoeffizient
%
\begin{align*}
  T = 0
\end{align*}

\subsection{Der \texorpdfstring{$\delta$}{Delta}-Potentialtopf} \index{$\delta$-Potentialtopf}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1)(5.5,0.5)
    \rput(0,0){
      \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-1)(1.5,0.5)[\color{DimGray} $x$,0][\color{DimGray} $V(x)$,180]
      \psline[linecolor=DarkOrange3](-1.3,0)(-0.5,0)(-0.5,-0.5)(0.5,-0.5)(0.5,0)(1.3,0)
    }
    \pnode(1,0.5){A}
    \pnode(3,0.5){B}
    \ncarc[arrows=->]{A}{B}
    %\naput{\color{DimGray} Idealisierung}
    \uput{0.2}[90](2,0.5){\color{DimGray} Idealisierung}
    \rput(4,0){
      \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-1)(1.5,0.5)[\color{DimGray} $x$,0][\color{DimGray} $V(x)$,0]
      \psline[linecolor=DarkOrange3](-1.3,0)(0,0)(0,-1)(0,0)(1.3,0)
    }
  \end{pspicture}
\end{figure}
%
Sei $V(x) = - \alpha \delta(x)$. Betrachte die stationäre Schrödingergleichung
%
\begin{align*}
  \left[ - \frac{\hbar^2}{2 m} \partial_x^2 - \alpha \delta(x) \right] \phi(x) &= E \phi(x) && \bigg| \int\limits_{-\varepsilon}^{\varepsilon} \mathrm{d}x \\
  - \frac{\hbar^2}{2 m} \left( \phi'(+\varepsilon) - \phi'(-\varepsilon) \right) - \alpha \phi(0) &= \mathcal{O}(\varepsilon)
\end{align*}
%
Sprungbedingung.

\minisec{$E < 0$}

Die Lösung für $x > 0$ lautet
%
\begin{align*}
  \phi(x) = A \exp(-\kappa x) \qquad \kappa = \sqrt{\frac{(-E) 2 m}{\hbar^2}}
\end{align*}
%
Für $x < 0$ lautet die Lösung
%
\begin{align*}
  \phi(x) = B \exp(\kappa x)
\end{align*}
%
Aus der Stetigkeit folgt
%
\begin{align*}
  \phi(+0) &= \phi(-0) \\
  A &= B
\end{align*}
%
Sprungbedingung
%
\begin{gather*}
  -\frac{\hbar^2}{2 m} \left( -\kappa A - \kappa A \right) - \alpha A = 0 \\
  \begin{aligned}
    \kappa &= \frac{m \alpha}{\hbar^2} \\
    E &= - \frac{\hbar^2}{2 m} \kappa^2 = - \frac{m}{2 \hbar^2} \alpha^2
  \end{aligned}
\end{gather*}
%
Es gibt also nur einen Energiezustand für $E < 0$! Es ist nicht trivial, dass ein solcher Energiezustand existiert.
%
\begin{align*}
  \phi(x) = A \exp(-\kappa |x|)
\end{align*}
%
Normierung
%
\begin{align*}
  1 = \int\limits_{-\infty}^{\infty} \mathrm{d}x \, \phi(x)^2 = 2 A^2 \int\limits_{-\infty}^{0} \mathrm{d}x \, \exp(-2 \kappa |x|) = \frac{A^2}{\kappa}
\end{align*}
%
\begin{align*}
  \boxed{\phi(x) = \sqrt{\kappa} \exp(-\kappa |x|)}
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-0.2)(1.5,1.5)
      \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-0.2)(1.5,1.5)[\color{DimGray} $x$,0][\color{DimGray} $\phi(x)^2$,180]
      \psplot[linecolor=DarkOrange3,plotpoints=200]{-1.5}{1.5}{2.71828182846^(-3*abs(x))}
  \end{pspicture}
\end{figure}


